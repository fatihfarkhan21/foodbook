import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:foodbook/view/view.dart';
import 'package:mdi/mdi.dart';

import 'page/manage_order_page.dart';
import 'page/menu_page/menu_page.dart';

class StaffAppPage extends StatefulWidget {
  @override
  _StaffAppPageState createState() => _StaffAppPageState();
}

DateTime backPress;

Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (backPress == null || 
        now.difference(backPress) > Duration(seconds: 2)) {
      backPress = now;
      Fluttertoast.showToast(
        msg: "Tekan lagi untuk keluar app",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.orange,
        textColor: Colors.white,
        fontSize: 16.0
      );
      return Future.value(false);
    }
    exit(0);
    return Future.value(true);
  }


class _StaffAppPageState extends State<StaffAppPage> {
  
  int currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: onWillPop,
    child : Scaffold(
      appBar:  AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Food Book",
          style: TextStyle(
              color: Colors.black, fontFamily: "Aquawax", fontSize: 27),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: <Widget>[
          IconButton(
            icon: Icon(
              Icons.swap_horiz,
              size: 30,
              color: Colors.black45,
            ),
            onPressed: (){
               Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context)=> AppPage()
                )
              );
            },
          )
        ],
      ),
      body: _buildBody(),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: onTapped,
        fixedColor: Colors.orange[400],
        //backgroundColor: Colors.black,
        unselectedItemColor: Colors.black54,
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.restaurant_menu),
            title: Text("Menu")
          ),
          BottomNavigationBarItem(
            icon: Icon(Mdi.bookMultipleVariant),
            title: Text("Meja")
          ),
          BottomNavigationBarItem(
            icon: Icon(Mdi.viewList),
            title: Text("Manage Order")
          ),
        ],
      ),
    )
  );
}
  _buildBody(){
    if(currentIndex == 0){
      return MenuPage();
    }else if(currentIndex == 1){
      return MejaPageView();
    }else {
      return ManageOrderPage();
    }
  }

  onTapped(int index){
    setState(() {
     currentIndex = index;
    });
  }
}