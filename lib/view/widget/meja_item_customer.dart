import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/view/widget/leading_image.dart';

class MejaItemCustomer extends StatelessWidget {
  final Meja meja;
  final bool isStaff;
  final String image;
  final ValueSetter<Meja> onEdit;
  final ValueSetter<Meja> onDelete;
  final ValueSetter<Meja> onSoldOut;
  MejaItemCustomer(
      {Key key,
      this.meja,
      this.isStaff,
      this.onEdit,
      this.onDelete,
      this.onSoldOut,
      this.image})
      : super(key: key);

  bool iSsoldOut = true;
  String soldOut = "Available";
  var colorbtn = Colors.orange[400];
  String kategori = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: LeadingImage(
            image: image,
          ),
          title: Row(
            children: <Widget>[
              Text(
                meja.nomorMeja,
                style: TextStyle(fontWeight: FontWeight.w900),
              ),
              meja.isTerpakai?
              Text("Terpakai"):
              Text("Availible")
            ],
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                meja.deskripsi,
                style: TextStyle(fontSize: 12),
              ),
              Text("Kapasitas "+"${meja.kapasitas}" + " Orang")
            ],
          ),
        ),
      ],
    );
  }
}
