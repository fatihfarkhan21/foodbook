import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/view/widget/details_menu.dart';

class ReservationItem extends StatefulWidget {
  final Meja meja;
  final bool isStaff;
  final String image;

  const ReservationItem({Key key, this.meja, this.isStaff, this.image})
      : super(key: key);
  @override
  _ReservationItemState createState() => _ReservationItemState();
}

class _ReservationItemState extends State<ReservationItem> {
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(top: 10, right: 10, left: 10),
        child: ListTile(
          onTap: () {
            showBottomSheet(
                context: context,
                builder: (BuildContext context) {
                  return DetailsPage(
                    namamenu: widget.meja.nomorMeja,
                    deskripsi: widget.meja.deskripsi,
                    harga: widget.meja.kapasitas.toString(),
                    image: widget.meja.image,
                  );
                });
          },
          leading: widget.meja.image == null
              ? Icon(
                  Icons.image,
                  size: 50,
                )
              : Image.network(widget.meja.image),
          title: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  widget.meja.nomorMeja,
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                widget.meja.isTerpakai ? Text("Terpakai") : Text("Available")
              ],
            ),
          ),
          subtitle: Container(
              height: 80,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      widget.meja.deskripsi,
                      style: TextStyle(fontSize: 12),
                    ),
                    Text(
                      "Capacity " +
                          widget.meja.kapasitas.toString() +
                          " Person",
                      style: TextStyle(
                          fontWeight: FontWeight.w700, color: Colors.black),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Card(
                          elevation: 1.0,
                          color: Colors.orange[400],
                          child: Container(
                              width: 80,
                              height: 28,
                              child: Center(
                                  child: GestureDetector(
                                child: Text("ORDER",
                                    style: TextStyle(
                                        fontWeight: FontWeight.w900,
                                        fontSize: 12,
                                        color: Colors.white)),
                                onTap: () {},
                              ))),
                        )
                      ],
                    )
                  ])),
        ));
  }
}
