import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/order_action.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';

BuildContext localContext;
var store;

class MejaCheckout extends StatefulWidget {
  final Pesanan pesanan;

  const MejaCheckout({Key key, this.pesanan}) : super(key: key);

  @override
  _MejaCheckoutState createState() => _MejaCheckoutState();
}

class _MejaCheckoutState extends State<MejaCheckout> {
  Meja dropdownValue;
  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<WestclicState>(context);
    return Container(
      height: 50,
      color: Colors.grey[100],
      child: StoreConnector<WestclicState, _DaftarMejaViewModel>(
        onInit: (store) {
          store.dispatch(LoadDaftarMeja());
        },
        converter: (store) {
          if (store.state.daftarMejaState is DaftarMejaNotLoaded ||
              store.state.daftarMejaState is DaftarMejaLoading) {
            return _DaftarMejaViewModel(isLoading: true, daftarMeja: null);
          }
          if (store.state.daftarMejaState is DaftarMejaLoaded) {
            return _DaftarMejaViewModel(
                isLoading: false,
                daftarMeja: (store.state.daftarMejaState as DaftarMejaLoaded)
                    .daftarMeja);
          }
          return _DaftarMejaViewModel(isLoading: true, daftarMeja: null);
        },
        builder: (context, daftarMejaVM) {          
          var _daftarmeja = daftarMejaVM.daftarMeja;
          return Container(
            width: 50,
              child: daftarMejaVM.isLoading
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  : Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Center(
                          child: DropdownButton<Meja>(
                              value: dropdownValue,
                              onChanged: (newValue) {
                                setState(() {
                                  dropdownValue = newValue;
                                  store.dispatch(MejaOrder(
                                      dropdownValue));
                                });
                              },
                              items: _daftarmeja.map((m) {
                                return DropdownMenuItem<Meja>(
                                  value: m,
                                  child: Text(
                                    m.nomorMeja,
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold
                                    ),
                                  ),
                                );
                              }).toList()),
                        ),
                      ],
                    ));
        },
      ),
    );
  }
}

class _DaftarMejaViewModel {
  final bool isLoading;
  final List<Meja> daftarMeja;

  _DaftarMejaViewModel({this.isLoading, this.daftarMeja});
}
