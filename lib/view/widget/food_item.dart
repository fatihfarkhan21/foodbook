import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/view/widget/leading_image.dart';

import 'catatan_page.dart';
import 'details_menu.dart';

class FoodItem extends StatefulWidget {
  final String namaMenu, deskripsi, harga, image;
  final Menu menu;
  final ValueSetter<Menu> onPlus;
  final ValueSetter<Menu> onNote;
  final ValueSetter<Menu> onMin;

  const FoodItem(
      {Key key,
      this.namaMenu,
      this.deskripsi,
      this.harga, 
      this.image,
      this.menu,
      this.onPlus,
      this.onNote,
      this.onMin})
      : super(key: key);
  @override
  _FoodItemState createState() => _FoodItemState();
}

class _FoodItemState extends State<FoodItem> {
  int jumlah = 0;
  String catatan;
  var colorBtnDone = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 3,
          child: ListTile(
        leading: GestureDetector(
                  child: CircleAvatar(
            child: widget.menu.image == null
                ? Icon(
                    Icons.image,
                    size: 30,
                  )
                : LeadingImage(
                    image: widget.menu.image,
                  ),
          ),
          onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => DetailsPage(
                        onPlus: widget.onPlus,
                        menu: widget.menu,
                        namamenu: widget.menu.namaNemu,
                        deskripsi: widget.menu.deskripsi,
                        harga: widget.menu.harga.toString(),
                        image: widget.menu.image,
                        isSoldout: widget.menu.isSoldOut,
                      )));
        },
        ),
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(
              widget.menu.namaNemu,
              style: TextStyle(fontWeight: FontWeight.w500),
            ),
            widget.menu.isSoldOut
                ? Container(
                    width: 80,
                    height: 30,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.red[300]),
                    child: Center(
                      child: Text(
                        "Sold Out",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w900,
                            fontSize: 12),
                      ),
                    ),
                  )
                : Container(
                    width: 80,
                    height: 30,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5),
                        color: Colors.white),
                    child: Center(
                      child: Text(
                        "Available",
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 12),
                      ),
                    ),
                  ),
          ],
        ),
        subtitle: Container(
          height: 90,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                widget.menu.deskripsi,
                style: TextStyle(fontSize: 12),
              ),
              Text(
                widget.menu.harga.toString(),
                style:
                    TextStyle(fontWeight: FontWeight.w700, color: Colors.black),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  SizedBox(height: 10),
                  widget.menu.isSoldOut
                      ? Card(
                          elevation: 5.0,
                          child: Container(
                            width: 80,
                            height: 30,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white),
                            child: Center(
                              child: Text(
                                "TAMBAH",
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.w900,
                                    fontSize: 12),
                              ),
                            ),
                          ),
                        )
                      : Container(
                          child: (widget.menu.jumlah == null ||
                                  widget.menu.jumlah == 0)
                              ? GestureDetector(
                                  onTap: () {
                                    widget.onPlus(widget.menu);
                                  },
                                  child: Card(
                                    elevation: 2.0,
                                    child: Container(
                                      width: 80,
                                      height: 30,
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(5),
                                          color: Colors.orange),
                                      child: Center(
                                        child: Text(
                                          "TAMBAH",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w900,
                                              fontSize: 12),
                                        ),
                                      ),
                                    ),
                                  ))
                              : Row(
                                  children: <Widget>[
                                    Card(
                                      elevation: 2.0,
                                      child: Container(
                                        height: 30,
                                        width: 30,
                                        child: Center(
                                          child: IconButton(
                                            icon: Icon(
                                              Icons.edit,
                                              color: Colors.orange,
                                              size: 15,
                                            ),
                                            onPressed: () {
                                              showBottomSheet(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return CatatanPage(
                                                      onNote: widget.onNote,
                                                      menu: widget.menu,
                                                    );
                                                  });
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                    Card(
                                      elevation: 2.0,
                                      child: Container(
                                        width: 80,
                                        height: 30,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            color: Colors.white),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceAround,
                                          children: <Widget>[
                                            GestureDetector(
                                              child: Container(
                                                height: 30,
                                                width: 20,
                                                child: Center(
                                                  child: Text(
                                                      " - ",
                                                      style: TextStyle(
                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.orange,
                                                          fontSize: 24),
                                                    ),
                                                ),
                                              ),
                                              onTap: () {
                                                widget.onMin(widget.menu);
                                              },
                                            ),
                                            Container(
                                              height: 20,
                                              width: 20,
                                              child: Center(
                                                child: Text(
                                                  widget.menu.jumlah.toString(),
                                                  style: TextStyle(
                                                      color: Colors.black,
                                                      fontWeight: FontWeight.bold,
                                                      fontSize: 15),
                                                ),
                                              ),
                                            ),
                                            GestureDetector(
                                               child: Container(
                                                height: 20,
                                                width: 20,
                                                child: Center(
                                                  child: Text(
                                                    " + ",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        fontWeight: FontWeight.bold,
                                                        color: Colors.orange),
                                                  ),
                                                ),
                                              ),
                                              onTap: () {
                                                widget.onPlus(widget.menu);
                                              },
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                        )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
