import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';

class MejaItem extends StatelessWidget {
  final Meja meja;
  final bool isStaff;
  final ValueSetter<Meja> onEdit;
  final ValueSetter<Meja> onDelete;
  final ValueSetter<Meja> onSoldOut;
  MejaItem(
      {Key key,
      this.meja,
      this.isStaff,
      this.onEdit,
      this.onDelete,
      this.onSoldOut})
      : super(key: key);

  bool iSsoldOut = true;
  String soldOut = "Available";
  var colorbtn = Colors.orange[400];
  String kategori = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
            ),
            child: meja.image==null?
          Icon(
            Icons.image,
            size: 50,
          )
          :Image.network(meja.image),
          ),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                meja.nomorMeja,
                style: TextStyle(fontWeight: FontWeight.w900),
              ),
             meja.isAvailable?
              Text("Available")
              :Text("Unavailable")
            ],
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                meja.deskripsi,
                style: TextStyle(fontSize: 12),
              ),
              Text("Kapasitas "+"${meja.kapasitas}" + " Orang")
            ],
          ),
        ),
        Row(
          children: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.edit),
              label: Text('Edit'),
              onPressed: () {
                onEdit(meja);
              },
            ),
            FlatButton.icon(
              icon: Icon(Icons.delete_forever),
              label: Text('Delete'),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context){
                    return AlertDialog(
                      title: Text(meja.nomorMeja),
                      content: Text("Are you sure to delete this menu ?"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("Delete"),
                          onPressed: (){
                            onDelete(meja);
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text("Cancel"),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                        )
                      ],
                    );
                  }
                );
              },
            ),
            FlatButton.icon(
              icon: Icon(Icons.check_circle),
              label: Text('Available'),
              onPressed: () {
                onSoldOut(meja);
              },
            )
          ],
        )
      ],
    );
  }
}
