import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/view/widget/detail_transaction_user.dart';


class TransactionPageItem extends StatelessWidget {
  final Pesanan pesanan;
  //final Menu menu;

  const TransactionPageItem(
      {Key key, this.pesanan,})
      : super(key: key);

   @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 3, top: 3),
      child: ListTile(
        title: cekStatus(pesanan),
        subtitle: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
           (pesanan.isResevasi != true)?
            Text(
              '${pesanan.tanggalCheckIn.substring(0, 10)} | ${pesanan.tanggalCheckIn.substring(11, 16)}',
              style: TextStyle(
                fontSize: 13,
              ),
            )
            :Text(
              '${pesanan.tanggalCheckIn}',
              style: TextStyle(
                fontSize: 13,
              ),
            ),
            Text("${pesanan.processStatus}",
                style: TextStyle(
                    color: Colors.green[500], fontWeight: FontWeight.w700))
          ],
        ),
        trailing: (pesanan.isResevasi == true)
            ? Text("")
            : Column(
                children: <Widget>[
                  Text("Total"),
                  Text("Rp.${pesanan.totalHarga}",
                      style: TextStyle(
                          fontSize: 14,
                          color: Colors.grey[900],
                          fontWeight: FontWeight.w700))
                ],
              ),
        onTap: () {
          showBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return DetailTransactionUser(pesanan: pesanan);
              });
        },
      ),
    );
  }

  cekStatus(Pesanan p) {
    if (p.isCatering == true) {
      return Text("Catering | Order by : ${pesanan.userName}",
          style: TextStyle(
              fontSize: 16, color: Colors.orange, fontWeight: FontWeight.w800));
    } else if (p.isResevasi == true) {
      return Text("Reservation | Order by : ${pesanan.userName}",
          style: TextStyle(
              fontSize: 16, color: Colors.orange, fontWeight: FontWeight.w800));
    } else {
      return Text("Dine in | Order by : ${pesanan.userName}",
          style: TextStyle(
              fontSize: 16, color: Colors.orange, fontWeight: FontWeight.w800));
    }
  }
}

