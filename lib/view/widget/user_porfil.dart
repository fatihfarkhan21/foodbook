import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/update_profile.dart';
import 'package:mdi/mdi.dart';

class UserProfile extends StatelessWidget {
  var store;
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<WestclicState>(context);
    return Container(
        child: StoreConnector<WestclicState, _UserProfileViewModel>(
      converter: (store) {
        if (!(store.state.authState is AuthenticationUnauthenticated ||
            store.state.authState is AuthenticationLoading)) {
          return _UserProfileViewModel(
              isLoading: false,
              userProfile:
                  (store.state.authState as AuthenticationAuthenticated).user);
        }
        return _UserProfileViewModel(isLoading: true, userProfile: null);
      },
      builder: (context, userProfileVM) {
        return Container(
          color: Colors.white,
          height: 80,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding:
                    EdgeInsets.only(top: 10, bottom: 10, right: 20, left: 20),
                child: Container(
                  constraints: BoxConstraints(maxWidth: 200, minWidth: 100),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(userProfileVM.userProfile.displayName,
                      overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 17, fontWeight: FontWeight.w800)),
                              Text(userProfileVM.userProfile.email,
                              overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 14)),  
                    userProfileVM.userProfile.nomerHp == null
                          ?  Container(
                        child:  Text("Belum ada nomor HP",
                          style: TextStyle(
                              fontSize: 14,)),
                        constraints: BoxConstraints(minWidth: 100, maxWidth: 200),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10)),
                      )
                          : Text(userProfileVM.userProfile.nomerHp,
                              style: TextStyle(
                                fontSize: 15,
                              )),
                    ],
                  ),
                ),
              ),
              Container(
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: Card(
                        elevation: 2.0,
                        child: GestureDetector(
                          child: Container(
                            width: 30,
                            height: 30,
                            child:
                                Icon(Mdi.pencil, color: Colors.grey, size: 30),
                          ),
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => UpdateProfile(
                                          user: userProfileVM.userProfile,
                                        )));
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 20),
                      child: Card(
                        elevation: 2.0,
                        child: GestureDetector(
                          child: Container(
                            width: 30,
                            height: 30,
                            child:
                                Icon(Mdi.logout, color: Colors.grey, size: 30),
                          ),
                          onTap: () {
                            showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text(
                                        userProfileVM.userProfile.displayName),
                                    content: Text(
                                        "Are you sure to Logout this aplication ?"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text("Logout"),
                                        onPressed: () {
                                          store.dispatch(LogOutAction());
                                          Navigator.pop(context);
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("Cancel"),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  );
                                });
                          },
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        );
      },
    ));
  }
}

class _UserProfileViewModel {
  final bool isLoading;
  final User userProfile;

  _UserProfileViewModel({this.isLoading, this.userProfile});
}
