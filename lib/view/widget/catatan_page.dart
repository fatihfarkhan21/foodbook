import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';

class CatatanPage extends StatefulWidget {
  final ValueSetter<Menu> onNote;
  final Menu menu;
  const CatatanPage({Key key, this.onNote, this.menu}) : super(key: key);
  @override
  _CatatanPageState createState() => _CatatanPageState();
}

class _CatatanPageState extends State<CatatanPage> {
  TextEditingController catatanController = TextEditingController();
  
  
  @override
  Widget build(BuildContext context) {
    if (widget.menu.catatan == null) {
      catatanController.text = "";
    } else {
      catatanController.text = widget.menu.catatan;
    }
    return Container(
      height: MediaQuery.of(context).size.height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        color: Colors.white,
      ),
      child: Column(
        children: <Widget>[
          Center(
              child: Column(
            children: <Widget>[
              SizedBox(height: 10),
              Container(
                width: 30,
                height: 2,
                color: Colors.black26,
              ),
              SizedBox(height: 3),
              Container(
                width: 30,
                height: 2,
                color: Colors.black26,
              )
            ],
          )),
          SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, top: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Tambahkan catatan untuk pesenan",
                  style: TextStyle(fontSize: 18, fontWeight: FontWeight.w900),
                ),
                Divider(
                  color: Colors.black87,
                ),
                TextField(
                  controller: catatanController,
                    decoration:
                        InputDecoration(hintText: "Contoh : Extra Pedas ya!"),
                    keyboardType: TextInputType.multiline,
                    maxLines: 8,
                    maxLength: 200,
                  ),
                GestureDetector(
                  child: Card(
                    elevation: 2.0,
                    child: Container(
                      width: 60,
                      height: 25,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.orange
                      ),
                      child: Center(
                        child: Text(
                          "DONE",
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.w900,
                              fontSize: 12),
                        ),
                      ),
                    ),
                  ),
                  onTap: (){
                  widget.menu.catatan = catatanController.text;
                    widget.onNote(widget.menu);
                    Navigator.pop(context);
                  },
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
