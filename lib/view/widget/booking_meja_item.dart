import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';

class BookingMejaItem extends StatelessWidget {
  final Meja meja;
  final ValueSetter<Meja> addMeja;
  final ValueSetter<Meja> removeMeja;

  const BookingMejaItem({Key key, this.meja, this.addMeja, this.removeMeja})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(
      children: <Widget>[
        Container(
          height: 20,
          width: 170,
          child: Center(
            child:(meja.isAvailable == true)? Text(
              "Available",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
            ):
            Text(
              "Booked",
              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        Card(
          elevation: 5,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            height: 85,
            width: 170,
            child: Row(
              children: <Widget>[
                (meja.image != null)
                    ? Container(
                        width: 85,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                                image: NetworkImage(meja.image),
                                fit: BoxFit.cover),
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                bottomLeft: Radius.circular(10))),
                      )
                    : Icon(
                        Icons.image,
                        size: 85,
                      ),
                Container(
                  width: 85,
                  decoration: BoxDecoration(
                      color: Colors.orange[400],
                      borderRadius: BorderRadius.only(
                          topRight: Radius.circular(10),
                          bottomRight: Radius.circular(10))),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Text(
                        meja.nomorMeja,
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      Text(meja.kapasitas.toString() + " Orang")
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        SizedBox(
          height: 5,
        ),
          buttonDetector(meja)
        ],
      )
    );
  }
  buttonDetector(Meja meja) {
    if (meja.isTerpakai == true) {
      return GestureDetector(
        child: Container(
          height: 45,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey,
          ),
          child: Center(
              child: Text("Cancel",
                  style: TextStyle(
                      fontFamily: "Roboto",
                      letterSpacing: 2,
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold))),
          width: 170,
        ),
        onTap: () {
          removeMeja(meja);
        },
      );
    } else if (meja.isAvailable != true) {
      return GestureDetector(
        child: Container(
          height: 45,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.grey,
          ),
          child: Center(
              child: Text("Booked",
                  style: TextStyle(
                      fontFamily: "Roboto",
                      letterSpacing: 2,
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold))),
          width: 170,
        ),
        onTap: () {},
      );
    } else {
      return GestureDetector(
        child: Container(
          height: 45,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            color: Colors.orange,
          ),
          child: Center(
              child: Text("Booking",
                  style: TextStyle(
                      fontFamily: "Roboto",
                      letterSpacing: 2,
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold))),
          width: 170,
        ),
        onTap: () {
          addMeja(meja);
        },
      );
    }
  }

}
