import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';

class CardItemOrder extends StatelessWidget {
  var store;
  BuildContext context;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 350,
        height: 55,
        child: StoreConnector<WestclicState, _DaftarMenuPesananViewModel>(
          converter: (store) {
            if (!(store.state.orderState is OrderNotCreated ||
                store.state.orderState is NewOrder)) {
              return _DaftarMenuPesananViewModel(
                  isLoading: false,
                  daftarMenu: (store.state.orderState as OrderUpdated).pesanan);
            }
            return _DaftarMenuPesananViewModel(isLoading: true, daftarMenu: null);
          },
          builder: (context, daftarPesananVM) {
            return Container(
                child: Center(
              child: (daftarPesananVM.daftarMenu == null)
                  ? Container()
                  : Container(
                      child: Center(
                          child: (daftarPesananVM.daftarMenu.menuPesanan.length ==
                                  0)
                              ? Text(
                                  "Jumlah item dipilih belum ada",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontFamily: "Roboto"),
                                )
                              :Center(
                                child: Container(
                                  width: 325,
                                  child: Card(
                                    color: Colors.orange[400],
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.circular(10)),
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          "${daftarPesananVM.daftarMenu.menuPesanan.length} | Menu",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontFamily: "Roboto"),
                                        ),
                                        Text(
                                          "Tab to see details order",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: "Roboto"),
                                        ),
                                      ],
                                    ),
                                  ),
                                ))),
                              ) 
            ));
          },
        ),
      ),
    );
  }
}

class _DaftarMenuPesananViewModel {
  final bool isLoading;
  final Pesanan daftarMenu;

  _DaftarMenuPesananViewModel({this.isLoading, this.daftarMenu});
}
