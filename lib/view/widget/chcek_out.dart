import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/button_tambah.dart';
import 'package:foodbook/view/widget/food_item.dart';
import 'package:foodbook/view/widget/pilih_meja.dart';

import '../view.dart';
import 'title_list_item.dart';
var store;
BuildContext context;

class CheckOut extends StatefulWidget {
    final ValueSetter<Menu> onPlus;
    final ValueSetter<Menu> onNote;
    final ValueSetter<Menu> onMin;

  const CheckOut({Key key, this.onPlus, this.onNote, this.onMin})
      : super(key: key);

  @override
  _CheckOutState createState() => _CheckOutState();
}

class _CheckOutState extends State<CheckOut> {
  final _formKey = GlobalKey<FormState>();
  AlamatDelivery _alamatDelivery;
  TextEditingController tcAlamat = TextEditingController();
  TextEditingController tcNoHp = TextEditingController();
  @override
  void initState() {
    if (_alamatDelivery != null) {
      tcAlamat.text = _alamatDelivery.alamat;
      tcNoHp.text = _alamatDelivery.nomorHp;
    } else {
      _alamatDelivery = AlamatDelivery(
        alamat: "",
        nomorHp: "",
        koordinat: "",
        nama: "",
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      body: StoreConnector<WestclicState, _DaftarMenuPesananViewModel>(
        converter: (store) {
          if (!(store.state.orderState is OrderNotCreated ||
              store.state.orderState is NewOrder)) {
            return _DaftarMenuPesananViewModel(
                isLoading: false,
                daftarPesanan:
                    (store.state.orderState as OrderUpdated).pesanan);
          }
          return _DaftarMenuPesananViewModel(
              isLoading: true, daftarPesanan: null);
        },
        builder: (context, daftarPesananVM) {
          return Center(
              child: SafeArea(
                  child: daftarPesananVM.daftarPesanan == null
                      ? Container(
                          child: Text("Sorry Belum ada Pesanan"),
                        )
                      : Container(
                          child: (daftarPesananVM
                                      .daftarPesanan.menuPesanan.length ==
                                  0)
                              ? Text("Sorry Belum ada Pesanan")
                              : Column(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 0,
                                      child: TitleItem(
                                        title: "CheckOut",
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Stack(
                                        children: <Widget>[
                                          ListView.builder(
                                            itemCount: daftarPesananVM
                                                .daftarPesanan
                                                .menuPesanan
                                                .length,
                                            itemBuilder:
                                                (BuildContext context, index) {
                                              return FoodItem(
                                                menu: daftarPesananVM
                                                    .daftarPesanan
                                                    .menuPesanan[index],
                                                image: "assets/ayam.jpg",
                                                onMin: widget.onMin,
                                                onNote: widget.onNote,
                                                onPlus: widget.onPlus,
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                    Card(
                                      elevation: 2,
                                      child: Container(
                                        height: 175,
                                        child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 20, left: 20),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceAround,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Text(
                                                      "Total Harga :",
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                    Text(
                                                      "Rp. " +
                                                          daftarPesananVM
                                                              .daftarPesanan
                                                              .totalHarga
                                                              .toString(),
                                                      style: TextStyle(
                                                          fontSize: 25),
                                                    ),
                                                  ],
                                                ),
                                                (daftarPesananVM.daftarPesanan
                                                            .isCatering !=
                                                        true)
                                                    ? Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: <Widget>[
                                                          Text(
                                                            "Pilih Nomor Meja :",
                                                            style: TextStyle(
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                          MejaCheckout(
                                                            pesanan:
                                                                daftarPesananVM
                                                                    .daftarPesanan,
                                                          ),
                                                        ],
                                                      )
                                                    : Form(
                                                        key: _formKey,
                                                        child: Column(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            TextFormField(
                                                              controller:
                                                                  tcAlamat,
                                                              validator:
                                                                  (value) {
                                                                if (value
                                                                    .isEmpty) {
                                                                  return "Input Alamat Tujuan";
                                                                }
                                                                return null;
                                                              },
                                                              onSaved: (val) {
                                                                _alamatDelivery
                                                                        .alamat =
                                                                    val;
                                                              },
                                                              decoration:
                                                                  InputDecoration(
                                                                      hintText:
                                                                          "Masukan Alamat Tujuan"),
                                                            ),
                                                            TextFormField(
                                                              controller:
                                                                  tcNoHp,
                                                              keyboardType:
                                                                  TextInputType
                                                                      .number,
                                                              validator:
                                                                  (value) {
                                                                if (value
                                                                    .isEmpty) {
                                                                  return "Input Nomor Hp";
                                                                }
                                                                return null;
                                                              },
                                                              onSaved: (val) {
                                                                _alamatDelivery
                                                                        .nomorHp =
                                                                    val;
                                                              },
                                                              decoration:
                                                                  InputDecoration(
                                                                      hintText:
                                                                          "Nomor Hp"),
                                                            ),
                                                          ],
                                                        ))
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 10.0, top: 10),
                                        child: _buttonOrder(
                                            daftarPesananVM.daftarPesanan,
                                            context)),
                                  ],
                                ),
                        )));
        },
      ),
    );
  }

  _buttonOrder(Pesanan p, BuildContext context) {
    if (p.isCatering == true) {
      if (tcAlamat.text != "" && tcNoHp.text != "") {
        return GestureDetector(
          child: ButtonMenu(
            colors: Colors.orange[400],
            height: 50,
            width: 350,
            text: "ORDER",
            size: 20,
          ),
          onTap: () {
            if (_formKey.currentState.validate()) {
              var form = _formKey.currentState;
              form.save();
              p.deliverTo = _alamatDelivery;
              store.dispatch(AddPesanan(pesanan: p));
              Navigator.pop(
                  context, MaterialPageRoute(builder: (context) => AppPage()));
            }
            Fluttertoast.showToast(
                msg: "Order Success",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 1,
                fontSize: 16.0);
          },
        );
      } else {
        return ButtonMenu(
          colors: Colors.grey,
          height: 50,
          width: 350,
          text: "ORDER",
          size: 20,
        );
      }
    } else {
      if (p.meja.length != 0) {
        return GestureDetector(
          child: ButtonMenu(
            colors: Colors.orange[400],
            height: 50,
            width: 350,
            text: "ORDER",
            size: 20,
          ),
          onTap: () {
            p.deliverTo = _alamatDelivery;
            store.dispatch(AddPesanan(pesanan: p));
            Fluttertoast.showToast(
                msg: "Order Success",
                toastLength: Toast.LENGTH_SHORT,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 1,
                fontSize: 16.0);
            Navigator.pushReplacement(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => AppPage()));
          },
        );
      } else {
        return ButtonMenu(
          colors: Colors.grey,
          height: 50,
          width: 350,
          text: "ORDER",
          size: 20,
        );
      }
    }
  }
}

class _DaftarMenuPesananViewModel {
  final bool isLoading;
  final Pesanan daftarPesanan;

  _DaftarMenuPesananViewModel({this.isLoading, this.daftarPesanan});
}
