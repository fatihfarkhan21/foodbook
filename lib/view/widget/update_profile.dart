import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';

class UpdateProfile extends StatefulWidget {
  final User user;
  UpdateProfile({Key key, this.user}) : super(key: key);
  @override
  _UpdateProfileState createState() => _UpdateProfileState();
}

class _UpdateProfileState extends State<UpdateProfile> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController namaController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController nomorHpController = new TextEditingController();

  String _nama, _nomorHp;
  User _user;

  var store;

  @override
  void initState() {
    _user = widget.user;
    namaController.text = _user.displayName;
    emailController.text = _user.email;
    nomorHpController.text = _user.nomerHp;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          GestureDetector(
            child: Center(
              child: Text("Edit   ",
                  style: TextStyle(
                      color: Colors.orange,
                      fontSize: 20,
                      fontWeight: FontWeight.w900)),
            ),
            onTap: () {
              if (_formKey.currentState.validate()) {
                _user.displayName = namaController.text;
                _user.nomerHp = nomorHpController.text;
                store.dispatch(UpdateProfileAction(user: _user));
                Navigator.pop(context);
              }
            },
          )
        ],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: Text("Edit Profile",
            style: TextStyle(color: Colors.black, fontWeight: FontWeight.w900)),
      ),
      body: Padding(
        padding: EdgeInsets.only(top: 20),
        child: Container(
            color: Colors.white,
            width: double.infinity,
            height: 270,
            child: Padding(
              padding: EdgeInsets.all(15),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Name",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w900),
                    ),
                    TextFormField(
                      
                      validator: (value) {
                        if (value.isEmpty) {
                          return "Enter Your Name";
                        }
                        else if(value.length > 40) {
                          return "Name cannot more than 40 characters";
                        }
                        return null;
                      },
                      onSaved: (val) {
                        _user.displayName = val;
                      },
                      controller: namaController,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Email",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w900),
                    ),
                    TextField(
                      decoration: InputDecoration(enabled: false),
                      controller: emailController,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w900),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Phone Number",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.w900),
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,
                      maxLength: 15,
                      onSaved: (val) {
                        _user.nomerHp = val;
                      },
                      controller: nomorHpController,
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w900),
                    ),
                  ],
                ),
              ),
            )),
      ),
    );
  }
}
