import 'package:flutter/material.dart';

class TitleItem extends StatelessWidget {
  final String title;

  const TitleItem({Key key, this.title}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 15, right: 15, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                title,
                style: TextStyle(
                  fontSize: 24,
                  fontFamily: "Roboto"
                ),
              ),
            ],
          ),
          Divider(
            color: Colors.black87,
          )
        ],
      ),
    );
  }
}
