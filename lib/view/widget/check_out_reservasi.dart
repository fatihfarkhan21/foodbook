import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/button_tambah.dart';

import '../app_page.dart';
import 'booking_meja_item.dart';
import 'title_list_item.dart';

var store;
BuildContext context;

class CheckOutReservasi extends StatefulWidget {
  final ValueSetter<Meja> addMeja;
  final ValueSetter<Meja> removeMeja;

  const CheckOutReservasi({Key key, this.addMeja, this.removeMeja})
      : super(key: key);

  @override
  _CheckOutReservasiState createState() => _CheckOutReservasiState();
}

class _CheckOutReservasiState extends State<CheckOutReservasi> {
  String _date = "Not set";
  String _time = "Not set";
  int index;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      body: StoreConnector<WestclicState, _DaftarMenuPesananViewModel>(
        converter: (store) {
          if (!(store.state.orderState is OrderNotCreated ||
              store.state.orderState is NewOrder)) {
            return _DaftarMenuPesananViewModel(
                isLoading: false,
                daftarPesanan:
                    (store.state.orderState as OrderUpdated).pesanan);
          }
          return _DaftarMenuPesananViewModel(
              isLoading: true, daftarPesanan: null);
        },
        builder: (context, daftarPesananVM) {
          return Center(
              child: SafeArea(
                  child: daftarPesananVM.daftarPesanan == null
                      ? Container(
                          child: Text("Sorry Belum ada Pesanan"),
                        )
                      : Container(
                          child: (daftarPesananVM.daftarPesanan.meja.length ==
                                  0)
                              ? Text("Sorry Belum ada Pesanan")
                              : Column(
                                  children: <Widget>[
                                    Expanded(
                                      flex: 0,
                                      child: TitleItem(
                                        title: "CheckOut",
                                      ),
                                    ),
                                    Expanded(
                                      flex: 1,
                                      child: Stack(
                                        children: <Widget>[
                                          GridView.builder(
                                            gridDelegate:
                                                SliverGridDelegateWithFixedCrossAxisCount(
                                                    crossAxisCount: 2),
                                            itemCount: daftarPesananVM
                                                .daftarPesanan.meja.length,
                                            itemBuilder:
                                                (BuildContext context, index) {
                                              return BookingMejaItem(
                                                meja: daftarPesananVM
                                                    .daftarPesanan.meja[index],
                                                addMeja: widget.addMeja,
                                                removeMeja: widget.removeMeja,
                                              );
                                            },
                                          ),
                                        ],
                                      ),
                                    ),
                                    Card(
                                      elevation: 2,
                                      child: Container(
                                        height: 175,
                                        child: Center(
                                          child: Padding(
                                            padding: const EdgeInsets.only(
                                                right: 20, left: 20),
                                            child: Column(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Text(
                                                      "Masukan Waktu dan tanggal Reservasi"),
                                                ),
                                                RaisedButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5.0)),
                                                  elevation: 4.0,
                                                  onPressed: () {
                                                    DatePicker.showDatePicker(
                                                        context,
                                                        theme: DatePickerTheme(
                                                          containerHeight:
                                                              210.0,
                                                        ),
                                                        showTitleActions: true,
                                                        minTime: DateTime(
                                                            2000, 1, 1),
                                                        maxTime: DateTime(
                                                            2022, 12, 31),
                                                        onConfirm: (date) {
                                                      print('confirm $date');
                                                      _date =
                                                          '${date.year} - ${date.month} - ${date.day}';
                                                      setState(() {});
                                                    },
                                                        currentTime:
                                                            DateTime.now(),
                                                        locale: LocaleType.en);
                                                  },
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    height: 50.0,
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        Row(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Icon(
                                                                    Icons
                                                                        .date_range,
                                                                    size: 18.0,
                                                                    color: Colors
                                                                        .teal,
                                                                  ),
                                                                  Text(
                                                                    " $_date",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .teal,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        fontSize:
                                                                            18.0),
                                                                  ),
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        Text(
                                                          "  Change",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.teal,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 18.0),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  color: Colors.white,
                                                ),
                                                SizedBox(
                                                  height: 20.0,
                                                ),
                                                RaisedButton(
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              5.0)),
                                                  elevation: 4.0,
                                                  onPressed: () {
                                                    DatePicker.showTimePicker(
                                                        context,
                                                        theme: DatePickerTheme(
                                                          containerHeight:
                                                              210.0,
                                                        ),
                                                        showTitleActions: true,
                                                        onConfirm: (time) {
                                                      print('confirm $time');
                                                      _time =
                                                          '${time.hour} : ${time.minute} : ${time.second}';
                                                      setState(() {});
                                                    },
                                                        currentTime:
                                                            DateTime.now(),
                                                        locale: LocaleType.en);
                                                    setState(() {});
                                                  },
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    height: 50.0,
                                                    child: Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceBetween,
                                                      children: <Widget>[
                                                        Row(
                                                          children: <Widget>[
                                                            Container(
                                                              child: Row(
                                                                children: <
                                                                    Widget>[
                                                                  Icon(
                                                                    Icons
                                                                        .access_time,
                                                                    size: 18.0,
                                                                    color: Colors
                                                                        .teal,
                                                                  ),
                                                                  Text(
                                                                    " $_time",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .teal,
                                                                        fontWeight:
                                                                            FontWeight
                                                                                .bold,
                                                                        fontSize:
                                                                            18.0),
                                                                  ),
                                                                ],
                                                              ),
                                                            )
                                                          ],
                                                        ),
                                                        Text(
                                                          "  Change",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.teal,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              fontSize: 18.0),
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                  color: Colors.white,
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    (_date == "Not set" && _time == "Not set")
                                        ? Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 10.0, top: 10),
                                            child: GestureDetector(
                                              child: ButtonMenu(
                                                colors: Colors.grey,
                                                height: 50,
                                                width: 350,
                                                text: "Booking",
                                                size: 20,
                                              ),
                                            ))
                                        : Padding(
                                            padding: const EdgeInsets.only(
                                                bottom: 10.0, top: 10),
                                            child: GestureDetector(
                                              child: ButtonMenu(
                                                colors: Colors.orange[400],
                                                height: 50,
                                                width: 350,
                                                text: "Booking",
                                                size: 20,
                                              ),
                                              onTap: () {
                                                daftarPesananVM.daftarPesanan
                                                        .tanggalCheckIn =
                                                    _date + " " + _time;
                                                store.dispatch(AddPesanan(
                                                    pesanan: daftarPesananVM
                                                        .daftarPesanan));
                                                Fluttertoast.showToast(
                                                    msg: "Order Success",
                                                    toastLength:
                                                        Toast.LENGTH_SHORT,
                                                    gravity:
                                                        ToastGravity.BOTTOM,
                                                    timeInSecForIos: 1,
                                                    fontSize: 16.0);
                                               Navigator.pushReplacement(context, MaterialPageRoute(
                                                  builder: (BuildContext context)=>AppPage()
                                                ));

                                              },
                                            )),
                                  ],
                                ),
                        )));
        },
      ),
    );
  }
}

class _DaftarMenuPesananViewModel {
  final bool isLoading;
  final Pesanan daftarPesanan;

  _DaftarMenuPesananViewModel({this.isLoading, this.daftarPesanan});
}
