import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';

class DetailsPage extends StatelessWidget {
  final ValueSetter<Menu> onPlus;
  final String namamenu, deskripsi, harga, image;
  final bool isSoldout;
  final Menu menu;
  const DetailsPage(
      {Key key,
      this.namamenu,
      this.deskripsi,
      this.harga,
      this.image,
      this.isSoldout,
      this.onPlus,
      this.menu})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text(namamenu),
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              SizedBox(height: 10),
              Card(
                child: Container(
                    height: 250,
                    width: 325,
                    child: image == null
                        ? Icon(
                            Icons.image,
                            size: 150,
                            color: Colors.grey,
                          )
                        : Image.network(
                            image,
                            fit: BoxFit.fill,
                          )),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                elevation: 5,
              ),
              ListTile(
                title: Text(namamenu),
                subtitle: Text(deskripsi),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: Divider(color: Colors.black),
              ),
              ListTile(
                title: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "Harga Makanan",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(harga),
                  ],
                ),
                subtitle: Padding(
                  padding: const EdgeInsets.only(top: 15),
                  child: (isSoldout == false)
                      ? FlatButton(
                          child: Card(
                            child: Container(
                              width: 325,
                              height: 50,
                              child: Center(
                                child: Text(
                                  "Tambah Ke keranjang".toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            elevation: 2.0,
                            color: Colors.orange,
                          ),
                          onPressed: () {
                            onPlus(menu);
                            Navigator.pop(context);
                          },
                        )
                      : Card(
                          child: Container(
                            width: 325,
                            height: 50,
                            child: Center(
                              child: Text(
                                "Tambah Ke keranjang".toUpperCase(),
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          elevation: 2.0,
                          color: Colors.white,
                        ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
