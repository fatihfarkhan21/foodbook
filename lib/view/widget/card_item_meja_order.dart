import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';

class CardItemOrderMeja extends StatelessWidget {
  var store;
  BuildContext context;
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 350,
        height: 55,
        child: StoreConnector<WestclicState, _DaftarMenuPesananViewModel>(
          converter: (store) {
            if (!(store.state.orderState is OrderNotCreated ||
                store.state.orderState is NewOrder)) {
              return _DaftarMenuPesananViewModel(
                  isLoading: false,
                  daftarMenu: (store.state.orderState as OrderUpdated).pesanan);
            }
            return _DaftarMenuPesananViewModel(
                isLoading: true, daftarMenu: null);
          },
          builder: (context, daftarPesananVM) {
            return Container(
                child: Center(
              child: (daftarPesananVM.daftarMenu == null)
                  ? Text(
                      "Jumlah item dipilih belum ada",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontFamily: "Roboto"),
                    )
                  : Container(
                      child: Center(
                          child: (daftarPesananVM.daftarMenu.meja.length == 0)
                              ? Text(
                                  "Jumlah item dipilih belum ada",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 16,
                                      fontFamily: "Roboto"),
                                )
                              : Container(
                                  child: Card(
                                    color: Colors.orange[400],
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                          "${daftarPesananVM.daftarMenu.meja.length} | Meja",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 16,
                                              fontFamily: "Roboto"),
                                        ),
                                        Text(
                                          "Ketuk untuk melihat details pesanan",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontFamily: "Roboto"),
                                        ),
                                      ],
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                  ),
                                  width: 350,
                                ))),
            ));
          },
        ),
      ),
    );
  }
}

class _DaftarMenuPesananViewModel {
  final bool isLoading;
  final Pesanan daftarMenu;

  _DaftarMenuPesananViewModel({this.isLoading, this.daftarMenu});
}
