import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/redux_states.dart';
class DetailManageOrder extends StatelessWidget {
  final Pesanan pesanan;

  const DetailManageOrder({Key key, this.pesanan}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var store = StoreProvider.of<WestclicState>(context);
    return Card(
      elevation: 2,
          child: Container(
        height: MediaQuery.of(context).size.height / 3,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          color: Colors.white,
        ),
        child: Column(
          children: <Widget>[
            Expanded(
              child: ListView(
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(top: 10, bottom: 20, left: 20, right: 20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                            Card(
                              elevation: 2,
                              child: Container(
                                width: 80,
                                height: 25,
                                child: FlatButton(
                                  color: Colors.orange,
                                  child: Text("Back",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w600)),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                            ),
                          ],
                        ),
                        Text("Detail Order",
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.w900)),
                        SizedBox(
                          height: 8,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Display Name "),
                            ),
                            Container(
                              constraints: BoxConstraints(minWidth: 80, maxWidth: 200),
                              child: Text(": ${pesanan.userName}", overflow: TextOverflow.ellipsis,), 
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Status "),
                            ),
                            Container(
                              child: Text(": ${pesanan.processStatus}"),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Type "),
                            ),
                            Container(
                              child: cekStatus(pesanan),
                            )
                          ],
                        ),
                        (pesanan.isResevasi != true)?
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Total "),
                            ),
                            Container(
                              child: Text(": Rp.${pesanan.totalHarga}"),
                            )
                          ],
                        )
                        :Container(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Order Date "),
                            ),
                            (pesanan.isResevasi != true)?
                            Container(
                              child: Text(": ${pesanan.tanggalCheckIn.substring(11,16)} ${pesanan.tanggalCheckIn.substring(0,10)}"),
                            )
                            :Container(
                              child: Text(": ${pesanan.tanggalCheckIn}"),
                            )
                          ],
                        ),
                        (pesanan.processStatus == "Completed" || pesanan.processStatus == "Completed ")?
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Completed Date "),
                            ),
                            pesanan.isResevasi != true?
                            Container(
                              child: Text(": ${pesanan.tanggalCheckOut.substring(11,16)} ${pesanan.tanggalCheckIn.substring(0,10)}"),
                            ):
                            Container(
                              child: Text((": ${pesanan.tanggalCheckOut.substring(0,10)} ${pesanan.tanggalCheckOut.substring(11,19)}"),
                            )
                            )

                          ],
                        )
                        :Container(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Table Number "),
                            ),
                            Expanded(
                                flex: 1,
                                child: ListView.builder(
                                  physics: const NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: pesanan.meja.length,
                                  itemBuilder: (BuildContext context, int index) {
                                    return Text(
                                        ": ${pesanan.meja[index].nomorMeja}");
                                  },
                                ))
                          ],
                        ),
                        (pesanan.isResevasi != true)?
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 100,
                              child: Text("Menu "),
                            ),
                            Expanded(
                                flex: 1,
                                child: ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: pesanan.menuPesanan.length,
                                  itemBuilder: (BuildContext context, int index) {
                                   return Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "=> ${pesanan.menuPesanan[index].namaNemu} x ${pesanan.menuPesanan[index].jumlah}"),
                                        (pesanan.menuPesanan[index].catatan!=null)?
                                        Text("note: ${pesanan.menuPesanan[index].catatan}")
                                        :Container()
                                      ],
                                    );   
                                  })
                            )],
                        )
                        :Container(),
                        SizedBox(
                              height: 10,
                            ),
                        Center(
                          child: (pesanan.processStatus == "Completed" || pesanan.processStatus == "Rejected"|| pesanan.deliveryStatus == "Archive")?
                          Card(
                                elevation: 0,
                                child: (pesanan.processStatus == "Completed " || pesanan.processStatus == "Rejected")?
                                Container()
                                :Container(
                                  width: 110,
                                  height: 30,
                                  child: FlatButton(
                                    color: Colors.orange,
                                    child: Text("Archive it",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600)),
                                    onPressed: () {
                                      pesanan.deliveryStatus = "Archive";
                                      pesanan.tanggalCheckOut = DateTime.now().toIso8601String();
                                      store.dispatch(UpdatePesanan(pesanan: pesanan));
                                      Navigator.pop(context);
                                    },
                                  ),
                                ),
                              )
                          :  Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                               Card(
                                elevation: 0,
                                child: Container(
                                  width: 100,
                                  height: 30,
                                  child: FlatButton(
                                    color: Colors.green,
                                    child: Text("Confirm",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600)),
                                    onPressed: () {
                                      if(pesanan.processStatus == "Pending"){
                                        pesanan.processStatus = "On-Process";
                                        store.dispatch(UpdatePesanan(pesanan: pesanan));
                                      }
                                      else if(pesanan.processStatus == "On-Process"){
                                        pesanan.processStatus = "Completed";
                                        pesanan.tanggalCheckOut = DateTime.now().toIso8601String();
                                        store.dispatch(UpdatePesanan(pesanan: pesanan));
                                      }
                                      Navigator.pop(context);
                                    },
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 30,
                              ),
                               Card(
                                elevation: 0,
                                child: Container(
                                  width: 100,
                                  height: 30,
                                  child: FlatButton(
                                    color: Colors.red,
                                    child: Text("Reject",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 14,
                                            fontWeight: FontWeight.w600)),
                                    onPressed: () {
                                     return showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text(
                                        "Reject alert!"),
                                    content: Text(
                                        "Are you sure to Reject this order ?"),
                                    actions: <Widget>[
                                      FlatButton(
                                        child: Text("Yes"),
                                        onPressed: () {
                                          if(pesanan.processStatus == "Pending"){
                                        pesanan.processStatus = "Rejected";
                                        store.dispatch(UpdatePesanan(pesanan: pesanan));
                                      }
                                      else if(pesanan.processStatus == "On-Process"){
                                        pesanan.processStatus = "Rejected";
                                        store.dispatch(UpdatePesanan(pesanan: pesanan));
                                      }
                                      Navigator.pop(context);
                                        },
                                      ),
                                      FlatButton(
                                        child: Text("No"),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  );
                                });
                                       
                                    },
                                  ),
                                ),
                              ),
                            ],
                          )
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
      
    );
  }
  _isReservasi(Pesanan p){
    if(p.isResevasi == true){
      
    }
  }
    cekStatus(Pesanan p) {
    if (p.isCatering == true) {
      return Text(": Catering");
    } else if (p.isResevasi == true) {
      return Text(": Reservation");
    } else {
      return Text(": Dine In");
    }
  }
}
