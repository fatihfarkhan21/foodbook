import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';

class MenuItem extends StatelessWidget {
  
  final Menu menu;
  final bool isStaff;
  final ValueSetter<Menu> onEdit;
  final ValueSetter<Menu> onDelete;
  final ValueSetter<Menu> onSoldOut;
  MenuItem(
      {Key key,
      this.menu,
      this.isStaff,
      this.onEdit,
      this.onDelete,
      this.onSoldOut})
      : super(key: key);

  bool iSsoldOut = true;
  String soldOut = "Available";
  var colorbtn = Colors.orange[400];
  String kategori = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ListTile(
          leading: Container(
            height: 50,
            width: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
            ),
            child: menu.image == null?
             Icon(
              Icons.image,
              size: 50,
            )
            : Image.network(menu.image)
          ),
          title: Text(
            menu.namaNemu,
            style: TextStyle(fontWeight: FontWeight.w900),
          ),
          subtitle: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                menu.deskripsi,
                style: TextStyle(fontSize: 12),
              ),
              Text(menu.harga.toString())
            ],
          ),
          trailing: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              menu.isCatering? Text("Catering"):
              Text("Dine in"),
              menu.isSoldOut? Text('Sold Out'): Text('Available'), 
            ],
          ),
        ),
        Row(
          children: <Widget>[
            FlatButton.icon(
              icon: Icon(Icons.edit),
              label: Text('Edit'),
              onPressed: () {
                onEdit(menu);
              },
            ),
            FlatButton.icon(
              icon: Icon(Icons.delete_forever),
              label: Text('Delete'),
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context){
                    return AlertDialog(
                      title: Text(menu.namaNemu),
                      content: Text("Are you sure to delete this menu ?"),
                      actions: <Widget>[
                        FlatButton(
                          child: Text("Delete"),
                          onPressed: (){
                            onDelete(menu);
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text("Cancel"),
                          onPressed: (){
                            Navigator.pop(context);
                          },
                        )
                      ],
                    );
                  }
                );
              },
            ),
            FlatButton.icon(
              icon: Icon(Icons.check_circle),
              label: Text('Sold out'),
              onPressed: () {
                onSoldOut(menu);
              },
            )
          ],
        )
      ],
    );
  }
}
