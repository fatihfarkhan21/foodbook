import 'package:flutter/material.dart';

class LeadingImage extends StatelessWidget {
  final String image;

  const LeadingImage({Key key, this.image}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 80,
        width: 80,
        child: CircleAvatar(
          backgroundColor: Colors.orange,
          child: ClipOval(
            child: Container(
              width: 50,
              height: 50,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: NetworkImage(image),
                  fit: BoxFit.cover
                )
              ),
            ),
          ),
        ),
        
    );
  }
}
