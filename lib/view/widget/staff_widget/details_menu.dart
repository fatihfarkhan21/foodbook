import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodbook/constants.dart';
import 'package:foodbook/model/model.dart';

class DetailsMenu extends StatelessWidget {
  final ValueSetter<Menu> onPlus;
  final String namamenu, deskripsi, harga, image;
  final bool isSoldout;
  final Menu menu;

  const DetailsMenu({Key key, this.onPlus, this.namamenu, this.deskripsi, this.harga, this.image, this.isSoldout, this.menu}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Colors.orange,
        body: SafeArea(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: size.width * 0.2, vertical: 10),
                child: Card(
                  elevation: 0,
                  shadowColor: Colors.black,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(size.width * 0.5)),
                  child: Container(
                    height: size.height * 0.3,
                    width: size.width * 0.5,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(size.width * 0.5),
                        image: DecorationImage(
                            image: AssetImage("assets/ayam.jpg"),
                            fit: BoxFit.cover)),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      color: Colors.white),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Icon(Icons.location_on),
                          SizedBox(
                            width: 10,
                          ),
                          Text(menu.namaNemu,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold))
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Column(
                              children: [],
                            ),
                          ),
                          ClipPath(
                            clipper: PricerCliper(),
                            child: Container(
                              alignment: Alignment.topCenter,
                              padding: EdgeInsets.symmetric(vertical: 10),
                              height: 66,
                              width: 65,
                              color: Colors.orange,
                              child: Text("Rp."+ "\n${menu.harga}",
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.bold)),
                            ),
                          )
                        ],
                      ),
                      Text(
                        menu.deskripsi,
                        style:
                            TextStyle(height: 1.5, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: size.height * 0.1),
                      Container(
                        // padding: EdgeInsets.all(20),
                        width: size.width * 0.8,
                        // it will cover 80% of total width
                        decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: null,
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  SizedBox(width: 10),
                                  Text(
                                    "Order Now",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}

class PricerCliper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    double ignoreHeight = 20;
    path.lineTo(0, size.height - ignoreHeight);
    path.lineTo(size.width / 2, size.height);
    path.lineTo(size.width, size.height - ignoreHeight);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
