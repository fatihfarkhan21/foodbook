import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';

import 'details_menu.dart';

class CardMenu extends StatelessWidget {
  final String namaMenu, deskripsi, harga, image;
  final Menu menu;
  final ValueSetter<Menu> onPlus;
  final ValueSetter<Menu> onNote;
  final ValueSetter<Menu> onMin;

  const CardMenu(
      {Key key,
      this.namaMenu,
      this.deskripsi,
      this.harga,
      this.image,
      this.menu,
      this.onPlus,
      this.onNote,
      this.onMin})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => DetailsMenu(
                      onPlus: onPlus,
                      menu: menu,
                      namamenu: menu.namaNemu,
                      deskripsi: menu.deskripsi,
                      harga: menu.harga.toString(),
                      image: menu.image,
                      isSoldout: menu.isSoldOut,
                    )));
      },
      child: Card(
        margin: EdgeInsets.only(
          top: 8.0,
          bottom: 8.0,
          left: 8.0,
          right: 8.0,
        ),
        elevation: 3,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        shadowColor: Colors.black,
        child: Container(
          height: size.height * 0.31,
          width: size.width * 0.4,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: size.height * 0.16,
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage("assets/ayamgoreng.jpg"),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                ),
              ),
              Container(
                height: size.height * 0.1,
                margin: EdgeInsets.only(left: 5, top: 3),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      child: Text(
                        menu.namaNemu,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(top: 3)),
                    SizedBox(
                      width: size.width * 0.4,
                      child: Text(menu.deskripsi,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              fontSize: 12, fontWeight: FontWeight.w800)),
                    ),
                    SizedBox(
                      child: Text(
                        "Rp. " + menu.harga.toString(),
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
