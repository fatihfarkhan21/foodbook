import 'package:flutter/material.dart';

class ButtonMenu extends StatelessWidget {
  final double height, width, size;
  final String text;
  final Color colors;

  const ButtonMenu({Key key, this.height, this.width, this.text, this.size, this.colors}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: colors,
      ),
      child: Center(
        child: Text(
          text,
          style: TextStyle(
            fontSize: size,
            letterSpacing: 2,
            fontWeight: FontWeight.w900,
            color: Colors.white
          ),
        ),
      ),
    );
  }
}
