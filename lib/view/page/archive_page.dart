import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/daftar_pesanan_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/manage_order_list_item.dart';

import '../staff_app_page.dart';

class ArchivePage extends StatelessWidget {
  BuildContext localContext;
  var store;
  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      appBar: AppBar(
          actions: <Widget>[
            GestureDetector(
              child: Center(
                child: Text(
                  "Exit    ",
                  style: TextStyle(
                      color: Colors.orange,
                      fontSize: 18,
                      fontWeight: FontWeight.w900),
                ),
              ),
              onTap: () {
                 Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => StaffAppPage()));
              },
            )
          ],
          backgroundColor: Colors.white,
          title: Text("Archive List",
              style:
                  TextStyle(color: Colors.black, fontWeight: FontWeight.w900))),
      body: StoreConnector<WestclicState, _DaftarPesananViewModel>(
        onInit: (store) {
          store.dispatch(LoadDaftarPesanan());
        },
        converter: (store) {
          if (store.state.daftarPesananState is DaftarPesananNotLoaded ||
              store.state.daftarPesananState is DaftarPesananLoading) {
            return _DaftarPesananViewModel(isLoading: true, daftarMenu: null);
          }
          if (store.state.daftarPesananState is DaftarPesananLoaded) {
            return _DaftarPesananViewModel(
                isLoading: false,
                daftarMenu:
                    (store.state.daftarPesananState as DaftarPesananLoaded)
                        .daftarPesanan);
          }
          return _DaftarPesananViewModel(isLoading: true, daftarMenu: null);
        },
        builder: (context, daftarPesananVM) {
          return daftarPesananVM.isLoading
              ? CircularProgressIndicator()
              : Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView(
                        children: <Widget>[
                          ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: daftarPesananVM.daftarMenu.length,
                            itemBuilder: (BuildContext context, int index) {
                              return (daftarPesananVM
                                          .daftarMenu[index].deliveryStatus ==
                                      "Archive")
                                  ? ManageOrderListItem(
                                      pesanan:
                                          daftarPesananVM.daftarMenu[index],
                                    )
                                  : Container();
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                );
        },
      ),
    );
  }
}

class _DaftarPesananViewModel {
  final bool isLoading;
  final List<Pesanan> daftarMenu;

  _DaftarPesananViewModel({this.isLoading, this.daftarMenu});
}
