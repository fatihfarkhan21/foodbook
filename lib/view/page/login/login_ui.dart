import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/carousel_pro/carousel_pro.dart';
import 'package:mdi/mdi.dart';

class Login extends StatelessWidget {
  var store;
  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/sl1.jpg"),
              fit: BoxFit.cover),
            ),
          ),
          Container(
            color: Colors.white,
            child: new Carousel(
              boxFit: BoxFit.cover,
              autoplay: true,
              animationDuration: Duration(milliseconds: 300),
              dotSize: 0.0,
              dotSpacing: 16.0,
              dotBgColor: Colors.transparent,
              showIndicator: false,
              overlayShadow: false,
              images: [
                AssetImage("assets/sl1.jpg"),
                AssetImage("assets/sl2.jpg"),
                AssetImage('assets/sl3.jpg'),
                AssetImage("assets/sl4.jpg"),
              ],
            ),
          ),
          Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    alignment: Alignment(0, 1),
                    child: Hero(
                      tag: "Treva",
                      child: Text(
                        "WestClic",
                        style: TextStyle(
                          fontFamily: 'Aquawax',
                          fontWeight: FontWeight.w900,
                          fontSize: 42,
                          letterSpacing: 0.4,
                          color: Colors.orange[400],
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Text(
                    "Food and Drink",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17.0,
                        fontWeight: FontWeight.bold,
                        fontFamily: "Sans",
                        letterSpacing: 1.3),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 70),
                  child: FlatButton(
                    child: Container(
                      height: 55,
                      width: 250,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        color: Colors.orange[400]
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(Mdi.google, color: Colors.white,),
                          Text(
                            "Sign In With Google",
                            style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold
                            ),
                          )
                        ],
                      ),
                    ),
                    onPressed: (){
                      store.dispatch(AuthWithGoogle());
                    },
                  ),
                )
              ]
            )
          ),
        ],
      ),
    );
  }
}