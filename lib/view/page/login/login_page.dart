import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/redux_actions/auth_action.dart';
import 'package:foodbook/redux_states/auth_state.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/view/view.dart';

import '../splash_screen_page.dart';
import 'login_ui.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  BuildContext localcontext;
  var store;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    localcontext = context;
    store = StoreProvider.of<WestclicState>(context);
    return StoreBuilder<WestclicState>(
      onInit: (store) => store.dispatch(AuthInit()),
      builder: (context, store) {
        if (store.state.authState is AuthenticationLoading) {
          return SplashScreen();
        } else if (store.state.authState is AuthenticationUnauthenticated) {
          return Login();
        } else {
          return AppPage();
        }
      },
    );
  }
}
