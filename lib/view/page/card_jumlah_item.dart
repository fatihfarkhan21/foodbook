import 'package:flutter/material.dart';

class JumlahItem extends StatelessWidget {
  final int jumlah;

  const JumlahItem({Key key, this.jumlah}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: ListTile(
        title: Text("$jumlah"),
      ),
      color: Colors.orange,
    );
  }
}
