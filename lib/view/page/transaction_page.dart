import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/model/user.dart';
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/daftar_pesanan_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/title_list_item.dart';
import 'package:foodbook/view/widget/transaction_page_item.dart';
import 'package:foodbook/view/widget/user_porfil.dart';

class TransactionPage extends StatefulWidget {
  final User user;

  const TransactionPage({Key key, this.user}) : super(key: key);
  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  BuildContext localContext;
  var store;
  //User _user;
  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
        body: StoreConnector<WestclicState, _DaftarPesananViewModel>(
            onInit: (store) {
      store.dispatch(LoadDaftarPesanan());
    }, converter: (store) {
      if (store.state.daftarPesananState is DaftarPesananNotLoaded ||
          store.state.daftarPesananState is DaftarPesananLoading) {
        return _DaftarPesananViewModel(isLoading: true, daftarMenu: null);
      }
      if (store.state.daftarPesananState is DaftarPesananLoaded) {
        return _DaftarPesananViewModel(
            isLoading: false,
            daftarMenu: (store.state.daftarPesananState as DaftarPesananLoaded)
                .daftarPesanan);
      }
      return _DaftarPesananViewModel(isLoading: true, daftarMenu: null);
    }, builder: (context, daftarPesananVM) {
      
      return daftarPesananVM.isLoading
          ? CircularProgressIndicator()
          : Column(
              children: <Widget>[
                UserProfile(),
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      TitleItem(title: "Incomplete"),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                      itemCount: daftarPesananVM.daftarMenu.length,
                      itemBuilder: (BuildContext context, int index) {
                        return
                        ((daftarPesananVM.daftarMenu[index].userId == widget.user.userId)&&(daftarPesananVM.daftarMenu[index].processStatus == "Pending"||daftarPesananVM.daftarMenu[index].processStatus == "On-Process"))?
                         TransactionPageItem(
                          pesanan: daftarPesananVM.daftarMenu[index],
                        )
                        :Container();
                      },
                    ),
                    TitleItem(title: "Completed"),
                      ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                      itemCount: daftarPesananVM.daftarMenu.length,
                      itemBuilder: (BuildContext context, int index) {
                        return
                        ((daftarPesananVM.daftarMenu[index].userName == widget.user.displayName)&&(daftarPesananVM.daftarMenu[index].processStatus == "Completed"))?
                         TransactionPageItem(
                          pesanan: daftarPesananVM.daftarMenu[index],
                        )
                        :Container();
                      }
                      )
                      
                    ],
                      
                  ),
                )
              ],
            );
    }));
  }
}

class _DaftarPesananViewModel {
  final bool isLoading;
  final List<Pesanan> daftarMenu;

  _DaftarPesananViewModel({this.isLoading, this.daftarMenu});
}
