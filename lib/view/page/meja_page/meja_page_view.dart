import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/meja_item.dart';

import 'meja_form.dart';
class MejaPageView extends StatelessWidget {
  BuildContext localContext;
  var store;
  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      body: StoreConnector<WestclicState, _DaftarMejaViewModel>(
        onInit: (store){
          store.dispatch(LoadDaftarMeja());
        },
        converter: (store){
          if(store.state.daftarMejaState is DaftarMejaNotLoaded ||
          store.state.daftarMejaState is DaftarMejaLoading){
            return _DaftarMejaViewModel(isLoading: true, daftarMeja: null);
          }
          if(store.state.daftarMejaState is DaftarMejaLoaded){
            return _DaftarMejaViewModel(
              isLoading: false,
              daftarMeja: (store.state.daftarMejaState as DaftarMejaLoaded)
              .daftarMeja
            );
          }
          return _DaftarMejaViewModel(isLoading: true, daftarMeja: null);
        },
        builder: (context, daftarMejaVm){
          return daftarMejaVm.isLoading ?
          Center(
            child: CircularProgressIndicator(),
          )
          : Column(
            children: <Widget>[
              Expanded(
                flex: 1,
                  child: ListView.builder(
                  itemCount: daftarMejaVm.daftarMeja.length,
                  itemBuilder: (context, index) {
                    return Container(
                      padding: EdgeInsets.only(top: 5, bottom: 5),
                      child: MejaItem(
                        meja: daftarMejaVm.daftarMeja[index],
                        onDelete: _onMenuDeleteHandler,
                        onEdit: _onMenuEdithandler,
                        onSoldOut: _onMenuTerpakaiHandler,
                      )
                    );
                  },
                )
              ),
            ],
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        backgroundColor: Colors.orange[400],
        onPressed: () {
          _navitageToMenuForm(context);
        },
      ),
    );
  }
  _navitageToMenuForm(BuildContext context, {Meja meja}) async {
    Meja mejaResult = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => MejaForm(meja: meja)));
    if (mejaResult != null) {
      if (mejaResult.id != null) {
        store.dispatch(UpdateMeja(meja: mejaResult));
      }else {
        store.dispatch(AddMeja(meja: mejaResult));
      }
    }
  }
  _onMenuDeleteHandler(Meja meja) async {
    store.dispatch(DeleteMeja(meja: meja));
  }
  

  _onMenuEdithandler(Meja meja) {
    _navitageToMenuForm(localContext, meja: meja);
    print('check');
  }
    _onMenuTerpakaiHandler(Meja meja) {
    if(meja.isAvailable){
      meja.isAvailable = false;
    }else{
      meja.isAvailable = true;
    }
    store.dispatch(UpdateMeja(meja: meja));
  }
}


class _DaftarMejaViewModel {
  final bool isLoading;
  final List<Meja> daftarMeja;

  _DaftarMejaViewModel({this.isLoading, this.daftarMeja});
}