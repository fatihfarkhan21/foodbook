import 'dart:io';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/view/widget/button_tambah.dart';
import 'package:path/path.dart' as Path;

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class MejaForm extends StatefulWidget {
  Meja meja;
  MejaForm({this.meja});
  @override
  _MejaFormState createState() => _MejaFormState();
}

class _MejaFormState extends State<MejaForm> {
  final _formKey = GlobalKey<FormState>();
  Meja _mejaItem;
  File _image;
  bool _isEdit;
  String _nomormeja;
  String _deskripsi;
  int _kapasitas;
  bool _isMejabaru = true;

  TextEditingController _nomorMeja = TextEditingController();
  TextEditingController _deskripsiMeja = TextEditingController();
  TextEditingController _kapasitasMeja = TextEditingController();

  Future getImage() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 85);
    setState(() {
      _image = image;
      print('image path: $_image');
      uploadImage(_image);
    });
  }

  @override
  void initState() {
    if (widget.meja != null) {
     
      _mejaItem = widget.meja;
      _isMejabaru = false;
      _nomorMeja.text = _mejaItem.nomorMeja;
      _deskripsiMeja.text = _mejaItem.deskripsi;
      _kapasitasMeja.text = _mejaItem.kapasitas.toString();
       _isEdit = true;
    } else {
      _mejaItem =
          Meja(isAvailable: false, isReseveable: false, isTerpakai: false);
      _isEdit = false;
    }
    super.initState();
  }

  Future uploadImage(var image) async {
    if (image == null) {
      return;
    } else {
      final storageReference =
          FirebaseStorage().ref().child("meja/${Path.basename(image.path)}");
      final StorageUploadTask uploadTask = storageReference.putFile(image);
      await uploadTask.onComplete;
      var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
      String _imageUrl = dowurl.toString();
      _mejaItem.image = _imageUrl;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("WestClic"),
        centerTitle: true,
        backgroundColor: Colors.black,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Form(
          key: _formKey,
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: ListView(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                      ),
                      Center(
                          child: (_mejaItem.image != null)
                              ? _isEdit == false
                                  ? Container(
                                      height: 150.9,
                                      width: 150.9,
                                      child: Image.file(_image))
                                  : Container(
                                      height: 150.9,
                                      width: 150.9,
                                      child: Image.network(_mejaItem.image))
                              : ListTile(
                                  title: Container(
                                    height: 150.9,
                                    width: 150.9,
                                    child: Icon(
                                      Icons.image,
                                      size: 150.9,
                                    ),
                                  ),
                                )),
                      FlatButton(
                        child: (widget.meja != null)
                            ? ButtonMenu(
                              colors: Colors.orange[400],
                                text: "Ganti Gambar",
                                height: 30,
                                width: 200,
                                size: 14,
                              )
                            : ButtonMenu(
                              colors: Colors.orange[400],
                                text: "Tambahkan Gambar",
                                height: 30,
                                width: 200,
                              ),
                        onPressed: () {
                          getImage();
                        },
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: "Nomor Meja",
                          ),
                          keyboardType: TextInputType.number,
                          controller: _nomorMeja,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Input Nomor Meja";
                            }
                            return null;
                          },
                          onSaved: (val) {
                            _mejaItem.nomorMeja = val;
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: TextFormField(
                          decoration: InputDecoration(
                            hintText: "Kapasitas Meja",
                          ),
                          keyboardType: TextInputType.number,
                          controller: _kapasitasMeja,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Input Kapasitas Orang perMeja";
                            }
                            return null;
                          },
                          onSaved: (val) {
                            _mejaItem.kapasitas = int.parse(val);
                          },
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 10),
                        child: TextFormField(
                          decoration:
                              InputDecoration(hintText: "Deskripsi Meja"),
                          controller: _deskripsiMeja,
                          validator: (value) {
                            if (value.isEmpty) {
                              return "Input Deskripsi Meja";
                            }
                            return null;
                          },
                          onSaved: (val) {
                            _mejaItem.deskripsi = val;
                          },
                        ),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            Container(
                              width: 160,
                              child: CheckboxListTile(
                                value: _mejaItem.isReseveable,
                                title: Text("Reservasi"),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (value) {
                                  setState(() {
                                    _mejaItem.isReseveable = value;
                                  });
                                },
                                activeColor: Colors.red,
                              ),
                            ),
                            
                          ],
                        ),
                      Padding(
                        padding: const EdgeInsets.only(top: 50),
                        child: FlatButton(
                          child: (widget.meja == null)
                              ? ButtonMenu(
                                colors: Colors.orange[400],
                                  height: 50,
                                  width: 400,
                                  text: "TAMBAH",
                                )
                              : ButtonMenu(
                                colors: Colors.orange[400],
                                  height: 50,
                                  width: 400,
                                  text: "UBAH",
                                ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              var form = _formKey.currentState;
                              form.save();
                              Navigator.pop(context, _mejaItem);
                            }
                          },
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
