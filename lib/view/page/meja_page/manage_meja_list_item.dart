import 'package:flutter/material.dart';
import 'package:foodbook/model/model.dart';

class ManageMejaListItem extends StatefulWidget {
  final Meja meja;
  // final String nomormeja;
  // final int kapasitas;
  // final String date;

  const ManageMejaListItem({this.meja});

  @override
  _ManageMejaListItemState createState() => _ManageMejaListItemState();
}

class _ManageMejaListItemState extends State<ManageMejaListItem> {
  String av = "Tersedia";
  String rv = "Sudah di Pesan";
  String us = "DiGunakan";
  String isrv = "Tersedia";
  bool boav = false;
  bool borv = false;
  bool bous = false;

  Color clav = Colors.orange;
  Color clrv = Colors.black;
  Color clus = Colors.black;

  void clickAV() {
    setState(() {
      isrv = av;
      clav = Colors.orange[400];
      borv = false;
      bous = false;
    });
  }

  void clickRV() {
    setState(() {
      isrv = rv;
      clrv = Colors.orange[400];
      boav = false;
      bous = false;
    });
  }

  void clickUS() {
    setState(() {
      isrv = us;
      clus = Colors.orange[400];
      boav = false;
      borv = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 10.0, right: 10.0),
        child: ListTile(
            leading: Container(
              height: 50,
              width: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(5),
              ),
              child: Icon(
                Icons.image,
                size: 50,
              ),
            ),
            title: Text(widget.meja.nomorMeja,
                style: TextStyle(
                    fontSize: 16,
                    color: Colors.black,
                    fontWeight: FontWeight.w900)),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "$isrv",
                  style: TextStyle(
                      fontSize: 12,
                      color: (isrv == av) ? Colors.green : Colors.orange[400],
                      fontWeight: FontWeight.w900),
                ),
                Text(
                  widget.meja.deskripsi,
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
                Text("${widget.meja.kapasitas} Orang",
                    style: TextStyle(fontSize: 11, color: Colors.grey[900])),
              ],
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                IconButton(
                  icon: CircleAvatar(
                    backgroundColor: (boav == true)
                        ? clav = Colors.orange[400]
                        : clav = Colors.black,
                    foregroundColor: Colors.white,
                    child: Text(
                      "Tersedia",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 8.0),
                    ),
                  ),
                  onPressed: () {
                    boav = true;
                    clav = Colors.orange[400];
                    clickAV();
                  },
                ),
                IconButton(
                  icon: CircleAvatar(
                    backgroundColor: (bous == true)
                        ? clus = Colors.orange[400]
                        : clus = Colors.black,
                    foregroundColor: Colors.white,
                    child: Text(
                      "Terpakai",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 8.0),
                    ),
                  ),
                  onPressed: () {
                    bous = true;
                    clickUS();
                  },
                ),
                IconButton(
                  icon: CircleAvatar(
                    backgroundColor: (borv == true)
                        ? clrv = Colors.orange[400]
                        : clrv = Colors.black,
                    foregroundColor: Colors.white,
                    child: Text(
                      "Terpesan",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 7.5),
                    ),
                  ),
                  onPressed: () {
                    borv = true;
                    clickRV();
                  },
                )
              ],
            )
          ),
    );
  }
}
