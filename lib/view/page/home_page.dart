import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/order_action.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/card_item_order.dart';
import 'package:foodbook/view/widget/chcek_out.dart';
import 'package:foodbook/view/widget/food_item.dart';
import 'package:foodbook/view/widget/staff_widget/card_menu.dart';

class HomePage extends StatelessWidget {
  //bool container = false;
  BuildContext localContext;
  var store;
  Pesanan p;
  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<WestclicState>(context);
    final orientation = MediaQuery.of(context).orientation;
    Size size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 14) /2;
    final double itemWidth = size.width / 2;

    return DefaultTabController(
      length: 3,
      child: Scaffold(
          appBar: AppBar(
              automaticallyImplyLeading: false,
              elevation: 0,
              backgroundColor: Colors.white,
              title: TabBar(
                labelColor: Colors.black,
                indicatorColor: Colors.white,
                labelStyle: TextStyle(color: Colors.white),
                tabs: <Widget>[
                  Container(
                    width: 100,
                    height: 30,
                    decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(10)),
                    child: Tab(
                      text: "Makanan",
                    ),
                  ),
                  Container(
                    width: 100,
                    height: 30,
                    decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(10)),
                    child: Tab(
                      text: "Minuman",
                    ),
                  ),
                  Container(
                    width: 100,
                    height: 30,
                    decoration: BoxDecoration(
                        border: Border.all(),
                        borderRadius: BorderRadius.circular(10)),
                    child: Tab(
                      text: "Snack",
                    ),
                  ),
                ],
              )),
          body: StoreConnector<WestclicState, _DaftarMenuViewModel>(
            onInit: (store) {
              store.dispatch(LoadDaftarMenu());
              store.dispatch(CreateOrder());
            },
            converter: (store) {
              if (store.state.daftarMenuState is DaftarMenuNotLoaded ||
                  store.state.daftarMenuState is DaftarMenuLoading) {
                return _DaftarMenuViewModel(isLoading: true, daftarMenu: null);
              }
              if (store.state.daftarMenuState is DaftarMenuLoaded) {
                return _DaftarMenuViewModel(
                    isLoading: false,
                    daftarMenu:
                        (store.state.daftarMenuState as DaftarMenuLoaded)
                            .daftarMenu);
              }
              return _DaftarMenuViewModel(isLoading: true, daftarMenu: null);
            },
            builder: (context, daftarMenuVM) {
              return Center(
                  child: daftarMenuVM.isLoading
                      ? CircularProgressIndicator()
                      : Stack(
                          children: <Widget>[
                            TabBarView(
                              
                              children: <Widget>[
                                Column(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: GridView.builder(
                                          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                            childAspectRatio: (itemWidth / itemHeight),
                                            crossAxisCount: (orientation == Orientation.portrait) ? 3 : 3
                                          ),
                                          
                                            itemCount:
                                                daftarMenuVM.daftarMenu.length,
                                            itemBuilder: (context, index) {
                                              var dataMenu = daftarMenuVM
                                                  .daftarMenu[index];
                                              if (!(store.state.orderState
                                                      is OrderNotCreated ||
                                                  store.state.orderState
                                                      is NewOrder)) {
                                                p = (store.state.orderState
                                                        as OrderUpdated)
                                                    .pesanan;

                                                if (p.menuPesanan != null) {
                                                  p.menuPesanan
                                                      .forEach((menuDiPesan) {
                                                    if (menuDiPesan.id ==
                                                        daftarMenuVM
                                                            .daftarMenu[index]
                                                            .id) {
                                                      dataMenu = menuDiPesan;
                                                      print("cek");
                                                    } else {
                                                      print("cek");
                                                    }
                                                  });
                                                }
                                              }
                                              return (daftarMenuVM
                                                              .daftarMenu[index]
                                                              .isCatering !=
                                                          true &&
                                                      daftarMenuVM
                                                              .daftarMenu[index]
                                                              .kategori ==
                                                          "makanan")
                                                  ?CardMenu(
                                                        menu: dataMenu,
                                                        onPlus:
                                                            _onAddMenuOrderHandler,
                                                        onNote: _onNoteHandler,
                                                        onMin:
                                                            _onRemoveOrderHandler,
                                                      )
                                                  : Container();
                                            })),
                                    GestureDetector(
                                      child: CardItemOrder(),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => CheckOut(
                                                      onMin:
                                                          _onRemoveOrderHandler,
                                                      onNote: _onNoteHandler,
                                                      onPlus:
                                                          _onAddMenuOrderHandler,
                                                    )));
                                      },
                                    )
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: ListView.builder(
                                            itemCount:
                                                daftarMenuVM.daftarMenu.length,
                                            itemBuilder: (context, index) {
                                              var dataMenu = daftarMenuVM
                                                  .daftarMenu[index];
                                              if (!(store.state.orderState
                                                      is OrderNotCreated ||
                                                  store.state.orderState
                                                      is NewOrder)) {
                                                p = (store.state.orderState
                                                        as OrderUpdated)
                                                    .pesanan;

                                                if (p.menuPesanan != null) {
                                                  p.menuPesanan
                                                      .forEach((menuDiPesan) {
                                                    if (menuDiPesan.id ==
                                                        daftarMenuVM
                                                            .daftarMenu[index]
                                                            .id) {
                                                      dataMenu = menuDiPesan;
                                                    } else {
                                                      print("cek");
                                                    }
                                                  });
                                                }
                                              }
                                              return (daftarMenuVM
                                                              .daftarMenu[index]
                                                              .isCatering !=
                                                          true &&
                                                      daftarMenuVM
                                                              .daftarMenu[index]
                                                              .kategori ==
                                                          "minuman")
                                                  ? Container(
                                                      padding: EdgeInsets.only(
                                                          top: 5, bottom: 5),
                                                      child: FoodItem(
                                                        menu: dataMenu,
                                                        onPlus:
                                                            _onAddMenuOrderHandler,
                                                        onNote: _onNoteHandler,
                                                        onMin:
                                                            _onRemoveOrderHandler,
                                                      ))
                                                  : Container();
                                            })),
                                    GestureDetector(
                                      child: CardItemOrder(),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => CheckOut(
                                                      onMin:
                                                          _onRemoveOrderHandler,
                                                      onNote: _onNoteHandler,
                                                      onPlus:
                                                          _onAddMenuOrderHandler,
                                                    )));
                                      },
                                    )
                                  ],
                                ),
                                Column(
                                  children: <Widget>[
                                    Expanded(
                                        flex: 1,
                                        child: ListView.builder(
                                            itemCount:
                                                daftarMenuVM.daftarMenu.length,
                                            itemBuilder: (context, index) {
                                              var dataMenu = daftarMenuVM
                                                  .daftarMenu[index];
                                              if (!(store.state.orderState
                                                      is OrderNotCreated ||
                                                  store.state.orderState
                                                      is NewOrder)) {
                                                p = (store.state.orderState
                                                        as OrderUpdated)
                                                    .pesanan;

                                                if (p.menuPesanan != null) {
                                                  p.menuPesanan
                                                      .forEach((menuDiPesan) {
                                                    if (menuDiPesan.id ==
                                                        daftarMenuVM
                                                            .daftarMenu[index]
                                                            .id) {
                                                      dataMenu = menuDiPesan;
                                                    } else {
                                                      print("cek");
                                                    }
                                                  });
                                                }
                                              }
                                              return (daftarMenuVM
                                                              .daftarMenu[index]
                                                              .isCatering !=
                                                          true &&
                                                      daftarMenuVM
                                                              .daftarMenu[index]
                                                              .kategori ==
                                                          "snack")
                                                  ? Container(
                                                      padding: EdgeInsets.only(
                                                          top: 5, bottom: 5),
                                                      child: FoodItem(
                                                        menu: dataMenu,
                                                        onPlus:
                                                            _onAddMenuOrderHandler,
                                                        onNote: _onNoteHandler,
                                                        onMin:
                                                            _onRemoveOrderHandler,
                                                      ))
                                                  : Container();
                                            })),
                                    GestureDetector(
                                      child: CardItemOrder(),
                                      onTap: () {
                                        Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) => CheckOut(
                                                      onMin:
                                                          _onRemoveOrderHandler,
                                                      onNote: _onNoteHandler,
                                                      onPlus:
                                                          _onAddMenuOrderHandler,
                                                    )));
                                      },
                                    )
                                  ],
                                ),
                              ],
                            )
                          ],
                        ));
            },
          )),
    );
  }

  _onAddMenuOrderHandler(Menu menu) {
    store.dispatch(AddMenuOrder(menu));
  }

  _onRemoveOrderHandler(Menu menu) {
    store.dispatch(RemoveMenuOrder(menu));
  }

  _onNoteHandler(Menu menu) {
    //print("cek" + catatan);
    store.dispatch(CatatanOrder(menu: menu));
  }
}

class _DaftarMenuViewModel {
  final bool isLoading;
  final List<Menu> daftarMenu;

  _DaftarMenuViewModel({this.isLoading, this.daftarMenu});
}
