import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/daftar_pesanan_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/page/rejected_page.dart';
import 'package:foodbook/view/widget/manage_order_list_item.dart';
import 'package:foodbook/view/widget/title_list_item.dart';

import 'archive_page.dart';
class ManageOrderPage extends StatelessWidget {
  BuildContext localContext;
  var store;
  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      body: StoreConnector<WestclicState, _DaftarPesananViewModel>(
        onInit: (store) {
          store.dispatch(LoadDaftarPesanan());
        },
        converter: (store) {
          if (store.state.daftarPesananState is DaftarPesananNotLoaded ||
              store.state.daftarPesananState is DaftarPesananLoading) {
            return _DaftarPesananViewModel(isLoading: true, daftarMenu: null);
          }
          if (store.state.daftarPesananState is DaftarPesananLoaded) {
            return _DaftarPesananViewModel(
                isLoading: false,
                daftarMenu:
                    (store.state.daftarPesananState as DaftarPesananLoaded)
                        .daftarPesanan);
          }
          return _DaftarPesananViewModel(isLoading: true, daftarMenu: null);
        },
        builder: (context, daftarPesananVM) {
          return daftarPesananVM.isLoading
              ? CircularProgressIndicator()
              : Column(
                  children: <Widget>[
                    Expanded(
                      child: ListView(
                        children: <Widget>[
                          SizedBox(
                            height: 15,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              FlatButton(
                                child: Container(
                                  width: 130,
                                  height: 35,
                                  decoration: BoxDecoration(
                                      color: Colors.orange,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Center(
                                      child: Text("Archive",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w800))),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => ArchivePage()));
                                },
                              ),
                              FlatButton(
                                child: Container(
                                  width: 130,
                                  height: 35,
                                  decoration: BoxDecoration(
                                      color: Colors.orange,
                                      borderRadius: BorderRadius.circular(5)),
                                  child: Center(
                                      child: Text("Rejected",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.w800))),
                                ),
                                onPressed: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RejectedPage()));
                                },
                              ),
                            ],
                          ),
                          TitleItem(
                            title: "Pending",
                          ),
                          ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: daftarPesananVM.daftarMenu.length,
                            itemBuilder: (BuildContext context, int index) {
                              return (daftarPesananVM
                                          .daftarMenu[index].processStatus ==
                                      "Pending" )
                                  ? ManageOrderListItem(
                                      pesanan:
                                          daftarPesananVM.daftarMenu[index],
                                    )
                                  : Container();
                            },
                          ),
                          TitleItem(
                            title: "On-Process",
                          ),
                          ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: daftarPesananVM.daftarMenu.length,
                            itemBuilder: (BuildContext context, int index) {
                              return (daftarPesananVM
                                          .daftarMenu[index].processStatus ==
                                      "On-Process")
                                  ? ManageOrderListItem(
                                      pesanan:
                                          daftarPesananVM.daftarMenu[index],
                                    )
                                  : Container();
                            },
                          ),
                          TitleItem(
                            title: "Completed",
                          ),
                          ListView.builder(
                            physics: const NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: daftarPesananVM.daftarMenu.length,
                            itemBuilder: (BuildContext context, int index) {
                              return (daftarPesananVM
                                          .daftarMenu[index].processStatus ==
                                      "Completed"&& daftarPesananVM
                                          .daftarMenu[index].deliveryStatus !=
                                      "Archive")
                                  ? ManageOrderListItem(
                                      pesanan:
                                          daftarPesananVM.daftarMenu[index],
                                    )
                                  : Container();
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                );
        },
      ),
    );
  }
}

class _DaftarPesananViewModel {
  final bool isLoading;
  final List<Pesanan> daftarMenu;

  _DaftarPesananViewModel({this.isLoading, this.daftarMenu});
}
