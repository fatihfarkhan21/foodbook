import 'dart:io';
import 'package:flutter/material.dart' as prefix0;
import 'package:foodbook/model/model.dart';
import 'package:foodbook/view/widget/button_tambah.dart';
import 'package:path/path.dart' as Path;

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class InputMenuPage extends StatefulWidget {
  Menu menu;

  InputMenuPage({Key key, this.menu}) : super(key: key);
  @override
  _InputMenuPageState createState() => _InputMenuPageState();
}

class _InputMenuPageState extends State<InputMenuPage> {
  final _formKey = GlobalKey<FormState>();
  Menu _menuItem;
  bool _isMenubaru = true;
  File _image;
  bool _isEdit;
  String _nama;
  String _deskripsi;
  String _kategori;
  String _harga;
  TextEditingController namaMenuController = TextEditingController();
  TextEditingController deskripsiController = TextEditingController();
  TextEditingController kategoriController = TextEditingController();
  TextEditingController hargaController = TextEditingController();

  Future getImage() async {
    var image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 85);
    setState(() {
      _image = image;
      print('image path: $_image');
      uploadImage(_image);
    });
  }

  @override
  void initState() {
    if (widget.menu != null) {
      _menuItem = widget.menu;
      _isMenubaru = false;
      namaMenuController.text = _menuItem.namaNemu;
      deskripsiController.text = _menuItem.deskripsi;
      kategoriController.text = _menuItem.kategori;
      hargaController.text = _menuItem.harga.toString();
      _isEdit = true;
    } else {
      _menuItem = Menu(
          isCatering: false, isDineIn: false, isPromo: false, isSoldOut: false);
      _isEdit = false;
    }
    super.initState();
  }

  Future uploadImage(var image) async {
    if (image == null) {
      return;
    } else {
      final StorageReference =
          FirebaseStorage().ref().child("menu/${Path.basename(image.path)}");
      final StorageUploadTask uploadTask = StorageReference.putFile(image);
      await uploadTask.onComplete;
      var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
      String _imageUrl = dowurl.toString();
      _menuItem.image = _imageUrl;
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:  AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Food Book",
          style: TextStyle(
              color: Colors.black, fontFamily: "Aquawax", fontSize: 27),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15),
        child: Form(
          key: _formKey,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20),
              child: Column(
                children: <Widget>[
                  Expanded(
                    child: ListView(
                      children: <Widget>[
                        Center(
                          child: (_menuItem.image != null)
                              ? _isEdit == false
                                  ? Container(
                                      height: 150.9,
                                      width: 150.9,
                                      child: Image.file(_image))
                                  : Container(
                                      height: 150.9,
                                      width: 150.9,
                                      child: Image.network(_menuItem.image))
                              : ListTile(
                                  title: Container(
                                    height: 150.9,
                                    width: 150.9,
                                    child: Icon(
                                      Icons.image,
                                      size: 150.9,
                                    ),
                                  ),
                                  subtitle:
                                      Center(child: Text("No Image Selected")),
                                ),
                        ),
                        SizedBox(height: 15),
                        FlatButton(
                          child: (widget.menu != null)
                              ? ButtonMenu(
                                  colors: Colors.orange[400],
                                  text: "Ganti Gambar",
                                  height: 30,
                                  width: 200,
                                )
                              : ButtonMenu(
                                  colors: Colors.orange[400],
                                  text: "Tambahkan Gambar",
                                  height: 30,
                                  width: 200,
                                ),
                          onPressed: () {
                            getImage();
                          },
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: TextFormField(
                            controller: namaMenuController,
                            decoration: InputDecoration(
                              hintText: "Nama Menu",
                            ),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Input Nama Menu";
                              }
                              return null;
                            },
                            onSaved: (val) {
                              _menuItem.namaNemu = val;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: TextFormField(
                            controller: deskripsiController,
                            decoration:
                                InputDecoration(hintText: "Deskripsi Menu"),
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Input Deskripsi Menu";
                              }
                              return null;
                            },
                            onSaved: (val) {
                              _menuItem.deskripsi = val;
                            },
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 200,
                              child: CheckboxListTile(
                                value: _menuItem.isPromo,
                                title: Text("Makanan"),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (value) {
                                  setState(() {
                                    _menuItem.isPromo = value;
                                  });
                                },
                                activeColor: Colors.red,
                              ),
                            ),
                            Container(
                              width: 180,
                              child: CheckboxListTile(
                                value: _menuItem.isDineIn,
                                title: Text("Minuman"),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (value) {
                                  setState(() {
                                    _menuItem.isDineIn = value;
                                  });
                                },
                                activeColor: Colors.red,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 200,
                              child: CheckboxListTile(
                                value: _menuItem.isPromo,
                                title: Text("Snack"),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (value) {
                                  setState(() {
                                    _menuItem.isPromo = value;
                                  });
                                },
                                activeColor: Colors.red,
                              ),
                            ),
                          ],
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: TextFormField(
                            controller: hargaController,
                            decoration: InputDecoration(hintText: "Harga Menu"),
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value.isEmpty) {
                                return "Input Harga Menu";
                              }
                              return null;
                            },
                            onSaved: (val) {
                              _menuItem.harga = int.parse(val);
                            },
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Container(
                              width: 200,
                              child: CheckboxListTile(
                                value: _menuItem.isCatering,
                                title: Text("Catering"),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (value) {
                                  setState(() {
                                    _menuItem.isCatering = value;
                                  });
                                },
                                activeColor: Colors.red,
                              ),
                            ),
                            Container(
                              width: 150,
                              child: CheckboxListTile(
                                value: _menuItem.isDineIn,
                                title: Text("Dine In"),
                                controlAffinity:
                                    ListTileControlAffinity.leading,
                                onChanged: (value) {
                                  setState(() {
                                    _menuItem.isDineIn = value;
                                  });
                                },
                                activeColor: Colors.red,
                              ),
                            ),
                          ],
                        ),
                        FlatButton(
                          child: (widget.menu == null)
                              ? ButtonMenu(
                                  colors: prefix0.Colors.orange[400],
                                  height: 50,
                                  width: 400,
                                  text: "TAMBAH",
                                )
                              : ButtonMenu(
                                  colors: Colors.orange[400],
                                  height: 50,
                                  width: 400,
                                  text: "UBAH",
                                ),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              var form = _formKey.currentState;
                              form.save();
                              Navigator.pop(context, _menuItem);
                            }
                          },
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
