import 'package:flutter/material.dart';

import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/menu_item.dart';

import 'input_menu_page.dart';

class MenuPage extends StatelessWidget {
  BuildContext localContext;
  var store;
  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      body: StoreConnector<WestclicState, _DaftarMenuViewModel>(
        onInit: (store) {
          store.dispatch(LoadDaftarMenu());
        },
        
        converter: (store) {
          if (store.state.daftarMenuState is DaftarMenuNotLoaded ||
              store.state.daftarMenuState is DaftarMenuLoading) {
            return _DaftarMenuViewModel(isLoading: true, daftarMenu: null);
          }
          if (store.state.daftarMenuState is DaftarMenuLoaded) {
            return _DaftarMenuViewModel(
                isLoading: false,
                daftarMenu: (store.state.daftarMenuState as DaftarMenuLoaded)
                    .daftarMenu);
          }
          return _DaftarMenuViewModel(isLoading: true, daftarMenu: null);
        },
        builder: (context, daftarMenuVM) {
          return daftarMenuVM.isLoading
              ? CircularProgressIndicator()
              : Column(
                  children: <Widget>[
                    Expanded(
                        flex: 1,
                        child: ListView.builder(
                          itemCount: daftarMenuVM.daftarMenu.length,
                          itemBuilder: (context, index) {
                            return Container(
                              padding: EdgeInsets.only(top: 5, bottom: 5),
                              child: MenuItem(
                                  menu: daftarMenuVM.daftarMenu[index],
                                  onDelete: _onMenuDeleteHandler,
                                  onEdit: _onMenuEdithandler,
                                  onSoldOut: _onMenuSoldOutHandler,
                                  isStaff: true),
                            );
                          },
                        )),
                        
                  ],
                );
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
        ),
        backgroundColor: Colors.orange[400],
        onPressed: () {
          _navitageToMenuForm(context);
        },
      ),
    );
  }

  _navitageToMenuForm(BuildContext context, {Menu menu}) async {
    Menu menuResult = await Navigator.push(context,
        MaterialPageRoute(builder: (context) => InputMenuPage(menu: menu)));
    if (menuResult != null) {
      if (menuResult.id == null) {
        store.dispatch(AddMenu(menu: menuResult));
      } else {
        store.dispatch(UpdateMenu(menu: menuResult));
      }
    }
  }

  _onMenuDeleteHandler(Menu menu) async {
    store.dispatch(DeleteMenu(menu: menu));
  }

  _onMenuEdithandler(Menu menu) {
    _navitageToMenuForm(localContext, menu: menu);
    print('check');
  }

  _onMenuSoldOutHandler(Menu menu) {
    if(menu.isSoldOut){
      menu.isSoldOut = false;
    }else{
      menu.isSoldOut = true;
    }
    store.dispatch(UpdateMenu(menu: menu));
  }
}

class _DaftarMenuViewModel {
  final bool isLoading;
  final List<Menu> daftarMenu;

  _DaftarMenuViewModel({this.isLoading, this.daftarMenu});
}
