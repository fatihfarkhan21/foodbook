import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

class SplashScreen extends StatefulWidget {
  @override
  _HalamanAwalState createState() => _HalamanAwalState();
}

class _HalamanAwalState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    //Timer(Duration(seconds: 5),()=> Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=> AppPage())));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Positioned(
            bottom: 0,
            right: 0,
            child: Image.asset(
              "assets/foodbook.png",
              width: size.width * 0.8,
              fit: BoxFit.contain,
            ),
          ),
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Center(
                    child: Text(
                  "FOOD BOOK",
                  textAlign: TextAlign.center,
                  style: GoogleFonts.bigShouldersText(
                    fontSize: 35,
                  ),
                ))
              ],
            ),
          ),
        ],
      ),
    );
  }
}
