import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/order_action.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/widget/booking_meja_item.dart';
import 'package:foodbook/view/widget/card_item_meja_order.dart';
import 'package:foodbook/view/widget/check_out_reservasi.dart';

class ReservasiPage extends StatelessWidget {
  @override
  BuildContext localContext;
  var store;
  Pesanan p;

  @override
  Widget build(BuildContext context) {
    localContext = context;
    store = StoreProvider.of<WestclicState>(context);
    return Scaffold(
        body: StoreConnector<WestclicState, _DaftarMejaViewModel>(
      onInit: (store) {
        store.dispatch(LoadDaftarMeja());
        store.dispatch(CreateOrder());
      },
      converter: (store) {
        if (store.state.daftarMejaState is DaftarMejaNotLoaded ||
            store.state.daftarMejaState is DaftarMejaLoading) {
          return _DaftarMejaViewModel(isLoading: true, daftarMeja: null);
        }
        if (store.state.daftarMejaState is DaftarMejaLoaded) {
          return _DaftarMejaViewModel(
              isLoading: false,
              daftarMeja:
                  (store.state.daftarMejaState as DaftarMejaLoaded).daftarMeja);
        }
        return _DaftarMejaViewModel(isLoading: true, daftarMeja: null);
      },
      builder: (context, daftarMejaVM) {
        return Center(
            child: daftarMejaVM.isLoading
                ? CircularProgressIndicator()
                : Stack(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          Expanded(
                              flex: 1,
                              child: GridView.builder(
                                itemCount: daftarMejaVM.daftarMeja.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 2),
                                itemBuilder: (context, index) {
                                  var dataMeja = daftarMejaVM.daftarMeja[index];
                                  if (!(store.state.orderState
                                          is OrderNotCreated ||
                                      store.state.orderState is NewOrder)) {
                                    p = (store.state.orderState as OrderUpdated)
                                        .pesanan;

                                    if (p.meja != null) {
                                      p.meja.forEach((mejaDiPesan) {
                                        if (mejaDiPesan.id ==
                                            daftarMejaVM.daftarMeja[index].id) {
                                          dataMeja = mejaDiPesan;
                                        }
                                      });
                                    }
                                  }
                                  return
                                  Padding(
                                    padding: const EdgeInsets.only(top: 5, bottom: 5),
                                      child: BookingMejaItem(
                                        meja: dataMeja,
                                        addMeja: _handleAddMejaOrder,
                                        removeMeja: _handleCancelAddMejaOrder,
                                      ),
                                    );
                                },
                              )
                          ),
                          GestureDetector(
                            child: CardItemOrderMeja(),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => CheckOutReservasi(
                                        addMeja: _handleAddMejaOrder,
                                        removeMeja: _handleCancelAddMejaOrder,
                                          )));
                            },
                          ),
                        ],
                      
                      ),
                      
                    ],
                  ));
      },
    ));
  }

  _handleAddMejaOrder(Meja meja) {
    store.dispatch(ReservationMeja(meja));
  }
    _handleCancelAddMejaOrder(Meja meja) {
    store.dispatch(CancelReservationMeja(meja));
  }
}

class _DaftarMejaViewModel {
  final bool isLoading;
  final List<Meja> daftarMeja;

  _DaftarMejaViewModel({this.isLoading, this.daftarMeja});
}
