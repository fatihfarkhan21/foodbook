import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:foodbook/model/user.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/view/page/catering_page.dart';
import 'package:foodbook/view/page/home_page.dart';
import 'package:foodbook/view/page/transaction_page.dart';
import 'package:foodbook/view/staff_app_page.dart';
import 'package:mdi/mdi.dart';

import 'page/reservasi_page.dart';

class AppPage extends StatefulWidget {
  @override
  _AppPageState createState() => _AppPageState();
}

DateTime backPress;

Future<bool> onWillPop() {
    DateTime now = DateTime.now();
    if (backPress == null || 
        now.difference(backPress) > Duration(seconds: 2)) {
      backPress = now;
      Fluttertoast.showToast(
        msg: "Tekan lagi untuk keluar app",
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        fontSize: 16.0
      );
      return Future.value(false);
    }
    exit(0);
    return Future.value(true);
  }

class _AppPageState extends State<AppPage> {
  int currentIndex = 0;
  var store;
  BuildContext context;

  @override
  Widget build(BuildContext context) {
    store = StoreProvider.of<WestclicState>(context);
    return WillPopScope(
      onWillPop: onWillPop,
    child : Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          "Food Book",
          style: TextStyle(
              color: Colors.black, fontFamily: "Aquawax", fontSize: 27),
        ),
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.white,
        actions: <Widget>[
          Container(
            child: StoreConnector<WestclicState, _UserProfileViewModel>(
              converter: (store) {
                if (!(store.state.authState is AuthenticationUnauthenticated ||
                    store.state.authState is AuthenticationLoading)) {
                  return _UserProfileViewModel(
                      isLoading: false,
                      userProfile:
                          (store.state.authState as AuthenticationAuthenticated)
                              .user);
                }
                return _UserProfileViewModel(
                    isLoading: true, userProfile: null);
              },
              builder: (context, userProfileVM) {
                return (userProfileVM.userProfile.isStaff == true)
                    ? IconButton(
                        icon: Icon(
                          Icons.swap_horiz,
                          size: 30,
                          color: Colors.black45,
                        ),
                        onPressed: () {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => StaffAppPage()));
                        },
                      )
                    : Text("");
              },
            ),

            // child:
          )
        ],
      ),
      body: StoreConnector<WestclicState, _UserProfileViewModel>(
              converter: (store) {
                if (!(store.state.authState is AuthenticationUnauthenticated ||
                    store.state.authState is AuthenticationLoading)) {
                  return _UserProfileViewModel(
                      isLoading: false,
                      userProfile:
                          (store.state.authState as AuthenticationAuthenticated)
                              .user);
                }
                return _UserProfileViewModel(
                    isLoading: true, userProfile: null);
              },
              builder: (context, userProfileVM){
                return _buildBody(userProfileVM.userProfile);
              },
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: onTapped,
        // fixedColor: Colors.black,
        selectedItemColor: Colors.orange,
        //backgroundColor: Colors.orange[400],
        unselectedItemColor: Colors.black45,
        type: BottomNavigationBarType.fixed,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(Mdi.food, size: 30),
              title: Text("Food",
                  style: TextStyle(fontSize: 12, fontFamily: "Roboto"))),
          BottomNavigationBarItem(
              icon: Icon(Mdi.bookOpenOutline),
              title: Text("Reservasi",
                  style: TextStyle(fontSize: 12, fontFamily: "Roboto"))),
          BottomNavigationBarItem(
              icon: Icon(Icons.local_shipping),
              title: Text("Catering",
                  style: TextStyle(fontSize: 12, fontFamily: "Roboto"))),
          BottomNavigationBarItem(
              icon: Icon(Mdi.viewList),
              title: Text("Order List",
                  style: TextStyle(fontSize: 12, fontFamily: "Roboto"))),
        ],
      ),
    )
  );
}

  _buildBody(User user) {
    if (currentIndex == 0) {
      return HomePage();
    } else if (currentIndex == 1) {
      return ReservasiPage();
    } else if (currentIndex == 2) {
      return CateringPage();
    } else {
      return TransactionPage(user: user,);
    }
  }

  onTapped(int index) {
    setState(() {
      currentIndex = index;
    });
  }
}

class _UserProfileViewModel {
  final bool isLoading;
  final User userProfile;

  _UserProfileViewModel({this.isLoading, this.userProfile});
}
