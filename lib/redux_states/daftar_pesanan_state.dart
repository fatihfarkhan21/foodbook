import 'package:equatable/equatable.dart';
import 'package:foodbook/model/pesanan.dart';

abstract class DaftarPesananState extends Equatable{
  DaftarPesananState([List props = const []]) : super(props);
}

class DaftarPesananNotLoaded extends DaftarPesananState{
  @override
  String toString() => 'DaftarPesananNotLoaded';
}

class DaftarPesananLoading extends DaftarPesananState{
  @override
  String toString() => 'DaftarPesananLoading';
}

class DaftarPesananLoaded extends DaftarPesananState{
  final List<Pesanan> daftarPesanan;

  DaftarPesananLoaded({this.daftarPesanan});
  @override
  String toString() => 'DaftarPesananLoaded: $daftarPesanan';
}