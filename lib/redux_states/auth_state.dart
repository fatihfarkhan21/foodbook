import 'package:equatable/equatable.dart';
import 'package:foodbook/model/user.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthState extends Equatable {
  final User user;
  AuthState(this.user, [List props = const []]) : super(props);
}

class AuthenticationUnauthenticated extends AuthState {
  AuthenticationUnauthenticated() : super(User());

  @override
  String toString() => 'AuthenticationUninitialized';
}

class AuthenticationLoading extends AuthState {
  AuthenticationLoading() : super(User());

  @override
  String toString() => 'AuthenticationLoading';
}

class AuthenticationAuthenticated extends AuthState {
  final User user;
  AuthenticationAuthenticated({this.user}) : super(null);

  @override
  String toString() => 'AuthenticationAuthenticated {userId: ${user.userId}}';
}

class AuthenticationError extends AuthState {
  final String errorInfo;
  AuthenticationError({this.errorInfo}) : super(null);
  @override
  String toString() => 'AuthenticationError { error: $errorInfo}';
}
