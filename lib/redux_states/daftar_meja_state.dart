import 'package:equatable/equatable.dart';
import 'package:foodbook/model/meja.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DaftarMejaState extends Equatable{
  DaftarMejaState([List props = const []]) : super(props);
}

class DaftarMejaNotLoaded extends DaftarMejaState{
  @override
  String toString() => 'DaftarMejaNotLoaded';
}
class DaftarMejaLoading extends DaftarMejaState{
  @override
  String toString() => 'DaftarMejaLoading';
}

class DaftarMejaLoaded extends DaftarMejaState{
  final List<Meja> daftarMeja;

  DaftarMejaLoaded({this.daftarMeja});
  @override
  String toString() => 'DaftarMejaLoaded: $daftarMeja';
}
