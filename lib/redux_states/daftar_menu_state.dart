import 'package:equatable/equatable.dart';
import 'package:foodbook/model/menu.dart';
import 'package:meta/meta.dart';

@immutable
abstract class DaftarMenuState extends Equatable{
  DaftarMenuState([List props = const []]) : super(props);
}

class DaftarMenuNotLoaded extends DaftarMenuState{
  @override
  String toString() => 'DaftarMenuNotLoaded';
}

class DaftarMenuLoading extends DaftarMenuState{
  @override
  String toString() => 'DaftarMenuLoading';
}

class DaftarMenuLoaded extends DaftarMenuState{
  final List<Menu> daftarMenu;

  DaftarMenuLoaded({this.daftarMenu});
  @override
  String toString() => 'DaftarMenuLoaded: $daftarMenu';
}