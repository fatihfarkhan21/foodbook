
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import 'daftar_pesanan_state.dart';
import 'order_state.dart';
import 'redux_states.dart';

@immutable
class WestclicState extends Equatable {
  final AuthState authState;
  final DaftarMejaState daftarMejaState;
  final DaftarMenuState daftarMenuState;
  final DaftarPesananState daftarPesananState;
  final OrderState orderState;
  

  WestclicState({this.daftarPesananState, this.orderState, this.authState, this.daftarMejaState, this.daftarMenuState}): super([]);

  WestclicState copyWith({
    AuthState authState,
    DaftarMejaState daftarMejaState,
    DaftarMenuState daftarMenuState,
    DaftarPesananState daftarPesananState,
    OrderState orderState
  }){
    return WestclicState(
      authState: authState ?? this.authState,
      daftarMejaState: daftarMejaState ?? this.daftarMejaState,
      daftarMenuState: daftarMenuState ?? this.daftarMenuState,
      daftarPesananState: daftarPesananState ?? this.daftarPesananState,
      orderState: orderState ?? this.orderState
      
    );
  }

  factory WestclicState.loading() => WestclicState(
    authState: AuthenticationUnauthenticated(),
    daftarMejaState: DaftarMejaNotLoaded(),
    daftarMenuState: DaftarMenuNotLoaded(),
    daftarPesananState: DaftarPesananNotLoaded(),
    orderState: OrderNotCreated()
  );

  @override
  String toString(){
    return 'WestclicState {AuthState: $authState, DaftarMejaState: $daftarMejaState, DaftarMenuState: $daftarMenuState, OrderState : $orderState, DaftarPesananState : $daftarPesananState}';
  }
}