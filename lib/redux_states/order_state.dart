import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:foodbook/model/pesanan.dart';

@immutable
abstract class OrderState extends Equatable {
  final Pesanan pesanan;
  OrderState(this.pesanan, [List props = const []]) : super(props);
}

class OrderNotCreated extends OrderState{
  OrderNotCreated() : super(Pesanan());

  @override
  String toString() => 'OrderNotCreated';
}
class NewOrder extends OrderState{
  final Pesanan pesanan;

  NewOrder(this.pesanan) : super(pesanan);
  @override
  String toString() => 'NewOrder';
}
class EditOrder extends OrderState{
  final Pesanan pesanan;

  EditOrder(this.pesanan) : super(pesanan);
  @override
  String toString() => 'EditOrder';
}

class OrderUpdated extends OrderState{
  final Pesanan pesanan;

  OrderUpdated(this.pesanan) : super(pesanan);
  @override
  String toString() => 'OrderUpdated';
}
class OrderCheckOuted extends OrderState{
  final Pesanan pesanan;

  OrderCheckOuted(this.pesanan) : super(pesanan);
  @override
  String toString() => 'OrderCheckOuted';
}
class Ordered extends OrderState{
  final Pesanan pesanan;

  Ordered(this.pesanan) : super(pesanan);
  @override
  String toString() => 'Ordered';
}
class OrderDelivered extends OrderState{
  final Pesanan pesanan;

  OrderDelivered(this.pesanan) : super(pesanan);
  @override
  String toString() => 'OrderDelivered';
}
class OrderPayed extends OrderState{
  final Pesanan pesanan;

  OrderPayed(this.pesanan) : super(pesanan);
  @override
  String toString() => 'OrderPayed';
}
class OrderFinished extends OrderState{
  final Pesanan pesanan;

  OrderFinished(this.pesanan) : super(pesanan);
  @override
  String toString() => 'OrderFinish';
}