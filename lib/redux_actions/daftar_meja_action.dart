
import 'package:foodbook/model/model.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class DaftarMejaAction extends Equatable{
  DaftarMejaAction([List props = const []]) : super(props);
}

class LoadDaftarMeja extends DaftarMejaAction{
  @override
  String toString() => 'LoadDaftarMeja';
}

class LoadDaftarMejaDone extends DaftarMejaAction{
  final List<Meja> daftarMeja;

  LoadDaftarMejaDone({this.daftarMeja});

  @override
  String toString() => 'LoadDaftarMejaDone';
}

class AddMeja extends DaftarMejaAction{
  final Meja meja;

  AddMeja({this.meja});

  @override
  String toString() => 'AddMeja:${meja.id}';
}

class DeleteMeja extends DaftarMejaAction{
  final Meja meja;

  DeleteMeja({this.meja});

  @override
  String toString() => 'DeleteMeja:${meja.id}';
}
class UpdateMeja extends DaftarMejaAction{
  final Meja meja;

  UpdateMeja({this.meja});

  @override
  String toString() => 'UpdateMenu:${meja.id}';
}
