import 'package:foodbook/model/user.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class AuthAction extends Equatable {
  AuthAction([List props = const []]) : super(props);
}

class AuthInit extends AuthAction {
  @override
  String toString() => 'AuthInit';
}

class AuthWithGoogle extends AuthAction {
  @override
  String toString() => 'AuthWithGoogle';
}

class AuthSuccess extends AuthAction {
  final User userProfile;

  AuthSuccess({this.userProfile});
  @override
  String toString() => 'AuthSuccess: ${userProfile.displayName}';
}


class LogOutAction extends AuthAction {
  @override
  String toString() => 'LogOutAction';
}
class UpdateProfileAction extends AuthAction{
  final User user;

  UpdateProfileAction({this.user});

  @override
  String toString() => 'UpdateProfile:${user.displayName}';
}
class LogOut extends AuthAction {
  @override
  String toString() => 'LogOut';
}

class AuthFail extends AuthAction {
  final String error;
  AuthFail(this.error);
  @override
  String toString() => 'AuthFail';
}

