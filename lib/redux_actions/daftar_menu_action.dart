import 'package:foodbook/model/menu.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

@immutable
abstract class DaftarMenuAction extends Equatable{
  DaftarMenuAction([List props = const []]) : super(props);
}

class LoadDaftarMenu extends DaftarMenuAction{
  @override
  String toString() => 'LoadDaftarMenu';
}

class LoadDaftarMenuDone extends DaftarMenuAction{
  final List<Menu> daftarMenu;

  LoadDaftarMenuDone({this.daftarMenu});

  @override
  String toString() => 'LoadDaftarMenuDone';
}

class AddMenu extends DaftarMenuAction{
  final Menu menu;

  AddMenu({this.menu});

  @override
  String toString() => 'AddMenu:${menu.id}';
}
class UpdateMenu extends DaftarMenuAction{
  final Menu menu;

  UpdateMenu({this.menu});

  @override
  String toString() => 'UpdateMenu:${menu.id}';
}

class DeleteMenu extends DaftarMenuAction{
  final Menu menu;

  DeleteMenu({this.menu});

  @override
  String toString() => 'DeleteMenu:${menu.id}';
}