import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:foodbook/model/pesanan.dart';

@immutable
abstract class DaftarPesananAction extends Equatable{
  DaftarPesananAction([List props = const []]) : super(props);
}

class LoadDaftarPesanan extends DaftarPesananAction{
  @override
  String toString() => 'LoadDaftarPesanan';
}

class LoadDaftarPesananDone extends DaftarPesananAction{
  final List<Pesanan> daftarMenu;

  LoadDaftarPesananDone({this.daftarMenu});

  @override
  String toString() => 'LoadDaftarPesanan';
}

class AddPesanan extends DaftarPesananAction{
  final Pesanan pesanan;

  AddPesanan({this.pesanan});

  @override
  String toString() => 'Addpesanan:${pesanan.id}';
}
class UpdatePesanan extends DaftarPesananAction{
  final Pesanan pesanan;

  UpdatePesanan({this.pesanan});

  @override
  String toString() => 'UpdatePesanan:${pesanan.id}';
}