import 'package:foodbook/model/meja.dart';
import 'package:foodbook/model/menu.dart';
import 'package:foodbook/model/pesanan.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';


@immutable
abstract class OrderAction extends Equatable {
  OrderAction([List props = const []]) : super(props);
}

class CreateOrder extends OrderAction{
  @override
  String toString() => 'CreateOrder';
}
class EditOrder extends OrderAction{
  final Pesanan pesanan;

  EditOrder(this.pesanan);
  @override
  String toString() => 'EditOrder';
}
class AddMenuOrder extends OrderAction{
  final Menu menu;

  AddMenuOrder(this.menu);
  
  @override
  String toString() => 'AddMenuOrder';
}
class RemoveMenuOrder extends OrderAction{
  final Menu menu;

  RemoveMenuOrder(this.menu);
  @override
  String toString() => 'RemoveMenuOrder';
}
class CatatanOrder extends OrderAction{
  final Menu menu;

  CatatanOrder({this.menu});
  @override
  String toString() => 'catatan order';
}
class MejaOrder extends OrderAction{
  final Meja meja;

  MejaOrder(this.meja);

}
class ReservationMeja extends OrderAction{
  final Meja meja;

  ReservationMeja(this.meja);
}
class CancelReservationMeja extends OrderAction{
  final Meja meja;

  CancelReservationMeja(this.meja);
}
class OrderUpdate extends OrderAction{
  final Pesanan pesanan;
  OrderUpdate(this.pesanan);
  @override
  String toString() => 'OrderUpdate';
}

class PostOrder extends OrderAction{
  @override
  String toString() => 'PostOrder';
}
class OrderDeliver extends OrderAction{
  @override
  String toString() => 'OrderDeliver';
}
class PayOrder extends OrderAction{
  @override
  String toString() => 'PayOrder';
}
class FinishOrder extends OrderAction{
  @override
  String toString() => 'FinishOrder';
}

