import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/repository/order_repository.dart';
import 'package:redux/redux.dart';
Middleware<WestclicState> daftarPesananMiddleware(){
  return (Store<WestclicState> store, action, NextDispatcher next){
    print('DaftarPesanan  Middleware: ' + action.runtimeType.toString());
    next(action); 

    if(action is AddPesanan){
      _handleAddMenu(store, action);
    }
  };
  
  
}
_handleAddMenu(Store<WestclicState> store, AddPesanan action) async {
 // MenuRepository.instance.addMenu(action.menu);
  OrderRepository.instance.addPesanan(action.pesanan);
}