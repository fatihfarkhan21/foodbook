import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/repository/repository.dart';
import 'package:redux/redux.dart';

Middleware<WestclicState> daftarMejaMiddleware() {
  return (Store<WestclicState> store, action, NextDispatcher next) {
    print('DaftarMeja  Middleware: ' + action.runtimeType.toString());

    next(action);

    if (action is AddMeja) {
      _handleAddMeja(store, action);
    } else if (action is UpdateMeja) {
      _handleUpdateMeja(store, action);
    }else if(action is DeleteMeja){
      _handleDeleteMeja(store, action);
    }
  };
}

_handleAddMeja(Store<WestclicState> store, AddMeja action) async {
  MejaRepository.instance.addMeja(action.meja);
}

_handleDeleteMeja(Store<WestclicState> store, DeleteMeja action) async {
  MejaRepository.instance.deleteMeja(action.meja);
}
_handleUpdateMeja(Store<WestclicState> store, UpdateMeja action) async {
  MejaRepository.instance.updateMeja(action.meja);
}
