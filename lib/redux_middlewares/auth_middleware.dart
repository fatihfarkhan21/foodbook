import 'package:foodbook/model/user.dart';
import 'package:foodbook/redux_actions/auth_action.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/repository/user_repository.dart';
import 'package:redux/redux.dart';

Middleware<WestclicState> authaMiddleware() {
  return (Store<WestclicState> store, action, NextDispatcher next) {
    print('Auth  Middleware: ' + action.runtimeType.toString());
    next(action);

    if (action is AuthInit) {
      _handleAuthInit(store, action);
    } else if (action is AuthWithGoogle) {
      _handleAuthWithGoogle(store, action);
    } else if (action is LogOutAction) {
      _handleLogOut(store, action);
    } else if (action is UpdateProfileAction) {
      _handleUpdateProfile(store, action);
    }
  };
}

_handleAuthInit(Store<WestclicState> store, AuthInit action) async {
  User user = await UserRepository.instance.currentUser();
  if (user == null) {
    store.dispatch(LogOut());
  } else {
    store.dispatch(AuthSuccess(userProfile: user));
  }
}

_handleAuthWithGoogle(Store<WestclicState> store, AuthWithGoogle action) async {
  User user = await UserRepository.instance.loginWithGoogle();
  if(user != null){
    store.dispatch(AuthSuccess(userProfile: user));
  }
  
}

_handleUpdateProfile(Store<WestclicState> store, UpdateProfileAction action) async {
  UserRepository.instance.updateProfile(action.user);
}

_handleLogOut(Store<WestclicState> store, LogOutAction action) async {
  await UserRepository.instance.logOut();
  store.dispatch(LogOut());
}
