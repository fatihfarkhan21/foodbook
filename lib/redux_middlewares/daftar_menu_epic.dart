import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/repository/menu_repository.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

Stream<dynamic> getDaftarMenuStream(
  Stream<dynamic> action,
  EpicStore<WestclicState> store
){
  final daftarMenuStream = Observable(action).ofType(TypeToken<LoadDaftarMenu>());
  return daftarMenuStream.switchMap((action){
    return Observable(MenuRepository.instance.getAllMenu()).map((daftarMenu) => LoadDaftarMenuDone(daftarMenu: daftarMenu));
  });

}