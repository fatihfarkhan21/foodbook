import 'package:foodbook/redux_actions/daftar_meja_action.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/repository/meja_repository.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';

Stream<dynamic> getDaftarMejaStream(
  Stream<dynamic> action,
  EpicStore<WestclicState> store
){
  final daftarMejaStream = Observable(action).ofType(TypeToken<LoadDaftarMeja>());
  return daftarMejaStream.switchMap((action){
    return Observable(MejaRepository.instance.getAllMeja()).map((daftarMeja) => LoadDaftarMejaDone(daftarMeja: daftarMeja));
  });
}
