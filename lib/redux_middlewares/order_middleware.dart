import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_actions/order_action.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:foodbook/repository/order_repository.dart';
import 'package:redux/redux.dart';

Middleware<WestclicState> ordermiddleware() {
  return (Store<WestclicState> store, action, NextDispatcher next) {
    next(action);
  
    if (action is AddMenuOrder) {
      _handleAddMenuOrder(store, action);
    } else if (action is RemoveMenuOrder) {
      _handleRemoveOrder(store, action);
    } else if (action is CatatanOrder) {
      _handleCatatanOrder(store, action);
    } else if (action is MejaOrder) {
      _handleAddmejaOrder(store, action);
    } else if (action is ReservationMeja) {
      _handleMejaOrder(store, action);
    } else if (action is UpdatePesanan) {
      _handleUpdatePesanan(store, action);
    } else if (action is CancelReservationMeja) {
      _handleCancelMejaOrder(store, action);
    }
  };
}

_handleAddMenuOrder(Store<WestclicState> store, AddMenuOrder action) {
  Menu m = action.menu;
  Pesanan p = store.state.orderState.pesanan;
  User u = store.state.authState.user;
  p.userId = u.userId;
  p.userName = u.displayName;
  
  p.processStatus = "Pending";
  p.tanggalCheckIn = DateTime.now().toIso8601String();
  p.onCreate = DateTime.now().toIso8601String();
  bool isnew = true;
  p.menuPesanan.forEach((menu) {
    if (menu.id == m.id) {
      var index = p.menuPesanan.indexOf(menu);
      p.menuPesanan[index].jumlah += 1;
      p.totalHarga += p.menuPesanan[index].harga;
      isnew = false;
    }
  });

  if (isnew) {
    p.menuPesanan.add(m);
    var index = p.menuPesanan.indexOf(m);
    if (m.isCatering == false) {
      p.isCatering = false;
      m.jumlah = 1;
      hitungTotalHarga(p, index);
    } else if (m.isCatering == true) {
      p.isCatering = true;
      m.jumlah = 6;
      if (p.totalHarga == null) {
        p.totalHarga = 0;
        p.totalHarga = p.menuPesanan[index].harga * m.jumlah;
      } else {
        p.totalHarga += p.menuPesanan[index].harga * m.jumlah;
      }
      p.deliverTo =
          AlamatDelivery(nama: "", nomorHp: "", koordinat: "", alamat: "");
    } 
  }
  store.dispatch(OrderUpdate(p));
}

String hitungTotalHarga(Pesanan p, int index) {
  var totalHarga;
  if (p.totalHarga == null) {
    p.totalHarga = 0;
    p.totalHarga += p.menuPesanan[index].harga;
  } else {
    p.totalHarga += p.menuPesanan[index].harga;
  }
  return p.totalHarga.toString();
}

_handleCatatanOrder(Store<WestclicState> store, CatatanOrder action) {
  Menu m = action.menu;
  Pesanan pesanan = store.state.orderState.pesanan;
  var index = pesanan.menuPesanan.indexOf(m);
  pesanan.menuPesanan[index].catatan = m.catatan;

  return OrderUpdated(pesanan);
}

_handleAddmejaOrder(Store<WestclicState> store, MejaOrder action) {
  Meja meja = action.meja;
  Pesanan p = store.state.orderState.pesanan;
  p.meja.add(meja);
  return OrderUpdated(p);
}

_handleRemoveOrder(Store<WestclicState> store, RemoveMenuOrder action) {
  Menu m = action.menu;
  Pesanan p = store.state.orderState.pesanan;
  bool isnew = true;
  p.menuPesanan.forEach((menu) {
    if (menu.id == m.id) {
      var index = p.menuPesanan.indexOf(menu);
      if (m.isCatering != true) {
        if (p.menuPesanan[index].jumlah > 1) {
          p.menuPesanan[index].jumlah -= 1;
          p.totalHarga -= p.menuPesanan[index].harga;
        } else {
          p.menuPesanan[index].jumlah -= 1;
          p.totalHarga -= p.menuPesanan[index].harga;
          p.menuPesanan.remove(m);
        }
        isnew = false;
      }else{
        if (p.menuPesanan[index].jumlah > 6) {
        p.menuPesanan[index].jumlah -= 1;
          p.totalHarga -= p.menuPesanan[index].harga;
        } else {
          p.menuPesanan[index].jumlah -= 6;
          p.menuPesanan.remove(m);
          isnew = false;
        }
      }
    }
  });

  store.dispatch(OrderUpdate(p));
}

_handleUpdatePesanan(Store<WestclicState> store, UpdatePesanan action) async {
  OrderRepository.instance.updatePesanan(action.pesanan);
}

_handleMejaOrder(Store<WestclicState> store, ReservationMeja action) {
  Meja meja = action.meja;
  Pesanan p = store.state.orderState.pesanan;

  User u = store.state.authState.user;
  p.userId = u.userId;
  p.userName = u.displayName;
  p.meja.add(meja);
  p.processStatus = "Pending";
  int index = p.meja.indexOf(meja);
  p.meja[index].isTerpakai = true;
  p.isResevasi = true;
  print("cek");
  store.dispatch(OrderUpdate(p));
}

_handleCancelMejaOrder( 
    Store<WestclicState> store, CancelReservationMeja action) {
  Meja meja = action.meja;
  Pesanan p = store.state.orderState.pesanan;
  int index = p.meja.indexOf(meja);
  p.meja[index].isTerpakai = false;

  p.meja.remove(meja);
  return OrderUpdated(p);
}
  