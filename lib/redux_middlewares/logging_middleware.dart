import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:redux_logging/redux_logging.dart';
import 'package:logging/logging.dart';

final westclicLogging = LoggingMiddleware<WestclicState>(
    logger: Logger('WestclicRedux')
      ..onRecord
          .where((record) => record.loggerName == 'WestclicRedux')
          .listen((loggingMiddlewareRecord) => print(loggingMiddlewareRecord)));
