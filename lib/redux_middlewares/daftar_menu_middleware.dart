import 'package:foodbook/redux_actions/daftar_menu_action.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/repository/menu_repository.dart';
import 'package:redux/redux.dart';

Middleware<WestclicState> daftarMenuMiddleware(){
  return (Store<WestclicState> store, action, NextDispatcher next){
    print('DaftarMenu  Middleware: ' + action.runtimeType.toString());
    
    next(action);
    
    if(action is AddMenu){
      _handleAddMenu(store, action);
    }else if(action is DeleteMenu){
      _handleDeleteMenu(store, action);
    }else if(action is UpdateMenu){
      _handleUpdateMenu(store, action);
    }
  };
}

_handleAddMenu(Store<WestclicState> store, AddMenu action) async {
  MenuRepository.instance.addMenu(action.menu);
}
_handleUpdateMenu(Store<WestclicState> store, UpdateMenu action) async {
  MenuRepository.instance.updateMenu(action.menu);
}
_handleDeleteMenu(Store<WestclicState> store, DeleteMenu action) async {
  MenuRepository.instance.deleteMenu(action.menu);
}