import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/westclic_state.dart';
import 'package:foodbook/repository/order_repository.dart';
import 'package:redux_epics/redux_epics.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/transformers.dart';

Stream<dynamic> getDaftarPesananStream(
  Stream<dynamic> action,
  EpicStore<WestclicState> store
){
  final daftarPesananStream = Observable(action).ofType(TypeToken<LoadDaftarPesanan>());
  return daftarPesananStream.switchMap((action){
    return Observable(OrderRepository.instance.getAllMenu()).map((daftarPesanan) => LoadDaftarPesananDone(daftarMenu: daftarPesanan));
  });

}