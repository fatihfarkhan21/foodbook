
import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_actions/order_action.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';
import 'package:redux/redux.dart';
import 'package:redux_epics/redux_epics.dart';
import 'daftar_pesanan_epic.dart';
import 'daftar_pesanan_middleware.dart';
import 'order_middleware.dart';
import 'redux_middlewares.dart';

final westclicMinddleware = createWestclicMiddleware();

List<Middleware<WestclicState>> createWestclicMiddleware(){
  return[
    westclicLogging,
    
    EpicMiddleware(westclicEpics),

    TypedMiddleware<WestclicState, AddMeja>(daftarMejaMiddleware()),
    TypedMiddleware<WestclicState, UpdateMeja>(daftarMejaMiddleware()),
    TypedMiddleware<WestclicState, DeleteMeja>(daftarMejaMiddleware()),

    TypedMiddleware<WestclicState, AddMenu>(daftarMenuMiddleware()),
    TypedMiddleware<WestclicState, UpdateMenu>(daftarMenuMiddleware()),
    TypedMiddleware<WestclicState, DeleteMenu>(daftarMenuMiddleware()),
    TypedMiddleware<WestclicState, AddPesanan>(daftarPesananMiddleware()),

    TypedMiddleware<WestclicState, AuthInit>(authaMiddleware()),
    TypedMiddleware<WestclicState, AuthWithGoogle>(authaMiddleware()),
    TypedMiddleware<WestclicState, LogOutAction>(authaMiddleware()),
    TypedMiddleware<WestclicState, UpdateProfileAction>(authaMiddleware()),

    TypedMiddleware<WestclicState, AddMenuOrder>(ordermiddleware()),
    TypedMiddleware<WestclicState, RemoveMenuOrder>(ordermiddleware()),
    TypedMiddleware<WestclicState, CatatanOrder>(ordermiddleware()),
    TypedMiddleware<WestclicState, MejaOrder>(ordermiddleware()),
    TypedMiddleware<WestclicState, ReservationMeja>(ordermiddleware()),
    TypedMiddleware<WestclicState, CancelReservationMeja>(ordermiddleware()),
    TypedMiddleware<WestclicState, UpdatePesanan>(ordermiddleware())



  ];
}

final westclicEpics = combineEpics<WestclicState>([
  TypedEpic<WestclicState, LoadDaftarMeja>(getDaftarMejaStream),
  TypedEpic<WestclicState, LoadDaftarMenu>(getDaftarMenuStream),
  TypedEpic<WestclicState, LoadDaftarPesanan>(getDaftarPesananStream),
]);