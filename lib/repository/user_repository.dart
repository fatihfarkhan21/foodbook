import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:foodbook/model/user.dart';
import 'package:google_sign_in/google_sign_in.dart';

class UserRepository {
  User user;
  FirebaseUser firebaseUser;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  GoogleSignInAccount googleAccount;
  GoogleSignInAuthentication googleAuth;
  final GoogleSignIn googleSignIn = GoogleSignIn();

  CollectionReference userProfileCR = Firestore.instance.collection("UserProfile");

  static final UserRepository instance = UserRepository();

  Future<User> currentUser() async {
    user = null;
    firebaseUser = await _firebaseAuth.currentUser();
    if (firebaseUser != null) {
      user = await getUserProfile(firebaseUser);
    }
    return user;
  }

  Future<User> loginWithGoogle() async {
    user = null;
    googleAccount = googleSignIn.currentUser;
    // if (googleAccount == null) {
    //   googleAccount = await googleSignIn.signInSilently();
    // }
    if (googleAccount == null) {
      googleAccount = await googleSignIn.signIn();
    }
    if (googleAccount != null) {
      googleAuth = await googleAccount.authentication;
      final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleAuth.accessToken, idToken: googleAuth.idToken);
      firebaseUser = (await _firebaseAuth.signInWithCredential(credential)).user;
      user = await getUserProfile(firebaseUser);
    }
    
    return user;
  }
  Future<void> updateProfile(User user) async {
  userProfileCR
      .document(user.userId)
      .updateData(user.toMap())
      .catchError((e) => print(e));
  }

  Future<User> getUserProfile(FirebaseUser firebaseUser) async {
    DocumentSnapshot snapshot = await userProfileCR.document(firebaseUser.uid).get();
    if(snapshot.data == null){
      this.user = User (displayName: firebaseUser.displayName, email: firebaseUser.email, userId: firebaseUser.uid, isStaff: false,);
      await userProfileCR.document(firebaseUser.uid).setData(user.toMap());
    }else{
      this.user = User.fromMap(snapshot.data);
    }
    return this.user;
  }

  Future<void> logOut() async {
    _firebaseAuth.signOut();
  }
}
