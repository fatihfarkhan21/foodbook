import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodbook/model/meja.dart';
import 'package:rxdart/rxdart.dart';

class MejaRepository {
  List<Meja> daftarMeja;
  CollectionReference mejaRepositoryCR = Firestore.instance.collection("Meja");

  static final MejaRepository instance = MejaRepository();

  Future<Meja> addMeja(Meja meja) async {
    meja.id = mejaRepositoryCR.document().documentID;
    mejaRepositoryCR
        .document(meja.id)
        .setData(meja.toMap())
        .catchError((e) => print(e));
    return meja;
  }

  Future<void> deleteMeja(Meja meja) async {
    mejaRepositoryCR.document(meja.id).delete().catchError((e) => print(e));
  }

  Future<void> updateMeja(Meja meja) async {
    mejaRepositoryCR
        .document(meja.id)
        .updateData(meja.toMap())
        .catchError((e) => print(e));
  }

  Stream<List<Meja>> getAllMeja() {
    Observable<List<Meja>> allMeja = Observable(mejaRepositoryCR
        .snapshots()
        .map((QuerySnapshot qs) => qs.documents
            .map((DocumentSnapshot ds) => Meja.fromMap(ds.data))
            .toList()));
    print(allMeja);
    print("cek");
    return allMeja;
  }

  Stream<List<Meja>> getAllMejaReservasi() {
    Observable<List<Meja>> allMeja = Observable(mejaRepositoryCR
        .where('isReseveable', isEqualTo: true)
        .snapshots()
        .map((QuerySnapshot qs) => qs.documents
            .map((DocumentSnapshot ds) => Meja.fromMap(ds.data))
            .toList()));
    return allMeja;
  }
}
