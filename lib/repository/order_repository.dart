import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodbook/model/pesanan.dart';
import 'package:rxdart/rxdart.dart';

class OrderRepository {
  List<Pesanan> daftarPesanan;
  CollectionReference orderRepositoryCR =
      Firestore.instance.collection("Pesanan");
  static final OrderRepository instance = OrderRepository();

  Future<Pesanan> addPesanan(Pesanan pesanan) async {
    pesanan.id = orderRepositoryCR.document().documentID;
    orderRepositoryCR
        .document(pesanan.id)
        .setData(pesanan.toMap())
        .catchError((e) => print(e));
    return pesanan;
  }

  Stream<List<Pesanan>> getAllMenu() {
    Observable<List<Pesanan>> allMenu = Observable(orderRepositoryCR
        .snapshots()
        .map((QuerySnapshot qs) => qs.documents
            .map((DocumentSnapshot ds) => Pesanan.fromMap(ds.data))
            .toList()));
    return allMenu;
  }
  

  Future<void> updatePesanan(Pesanan pesanan) async {
    orderRepositoryCR
        .document(pesanan.id)
        .updateData(pesanan.toMap())
        .catchError((e) => print(e));
  }
}
