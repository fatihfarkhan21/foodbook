import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:foodbook/model/menu.dart';
import 'package:rxdart/rxdart.dart';

class MenuRepository{
  List<Menu> daftarMenu;
  CollectionReference menuRepositoryCR = Firestore.instance.collection("Menu");

  static final MenuRepository instance = MenuRepository();

  Future<Menu> addMenu(Menu menu) async {
    menu.id = menuRepositoryCR.document().documentID;
    menuRepositoryCR
        .document(menu.id)
        .setData(menu.toMap())
        .catchError((e) => print(e));
    return menu;
  }

  Future<void> deleteMenu(Menu menu) async {
    menuRepositoryCR.document(menu.id).delete().catchError((e) => print(e));
  }

  Future<void> updateMenu(Menu menu) async {
    menuRepositoryCR
        .document(menu.id)
        .updateData(menu.toMap())
        .catchError((e) => print(e));
  }

  Stream<List<Menu>> getAllMenu() {
    Observable<List<Menu>> allMenu = Observable(menuRepositoryCR
        .snapshots()
        .map((QuerySnapshot qs) => qs.documents
            .map((DocumentSnapshot ds) => Menu.fromMap(ds.data))
            .toList()));
    return allMenu;
  }




}