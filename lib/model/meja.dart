import 'dart:convert';

class Meja {
    String id;
    String nomorMeja;
    String status;
    int kapasitas;
    String deskripsi;
    String image;
    bool isAvailable;
    bool isReseveable;
    bool isTerpakai;

    Meja({
        this.id,
        this.nomorMeja,
        this.status,
        this.kapasitas,
        this.deskripsi,
        this.image,
        this.isAvailable,
        this.isReseveable,
        this.isTerpakai,
    });

    factory Meja.fromJson(String str) => Meja.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Meja.fromMap(Map<String, dynamic> json) =>  Meja(
        id: json["id"] == null ? null : json["id"],
        nomorMeja: json["nomorMeja"] == null ? null : json["nomorMeja"],
        status: json["status"] == null ? null : json["status"],
        kapasitas: json["kapasitas"] == null ? null : json["kapasitas"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        image: json["image"] == null ? null : json["image"],
        isAvailable: json["isAvailable"] == null ? null : json["isAvailable"],
        isReseveable: json["isReseveable"] == null ? null : json["isReseveable"],
        isTerpakai: json["isTerpakai"] == null ? null : json["isTerpakai"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "nomorMeja": nomorMeja == null ? null : nomorMeja,
        "status": status == null ? null : status,
        "kapasitas": kapasitas == null ? null : kapasitas,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "image": image == null ? null : image,
        "isAvailable": isAvailable == null ? null : isAvailable,
        "isReseveable": isReseveable == null ? null : isReseveable,
        "isTerpakai": isTerpakai == null ? null : isTerpakai,
    };
}