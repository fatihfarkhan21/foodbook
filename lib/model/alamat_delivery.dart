import 'dart:convert';

class AlamatDelivery {
    String nama;
    String alamat;
    String koordinat;
    String nomorHp;

    AlamatDelivery({
        this.nama,
        this.alamat,
        this.koordinat,
        this.nomorHp,
    });

    factory AlamatDelivery.fromJson(String str) => AlamatDelivery.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory AlamatDelivery.fromMap(Map<String, dynamic> json) => AlamatDelivery(
        nama: json["nama"] == null ? null : json["nama"],
        alamat: json["alamat"] == null ? null : json["alamat"],
        koordinat: json["koordinat"] == null ? null : json["koordinat"],
        nomorHp: json["nomorHp"] == null ? null : json["nomorHp"],
    );

    Map<String, dynamic> toMap() => {
        "nama": nama == null ? null : nama,
        "alamat": alamat == null ? null : alamat,
        "koordinat": koordinat == null ? null : koordinat,
        "nomorHp": nomorHp == null ? null : nomorHp,
    };
}