// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

import 'dart:convert';

import 'alamat_delivery.dart';

class User {
    String userId;
    String userName;
    String displayName;
    String status;
    bool isStaff;
    String email;
    String nomerHp;
    List<AlamatDelivery> alamatDelivery;

    User({
        this.userId,
        this.userName,
        this.displayName,
        this.status,
        this.isStaff,
        this.email,
        this.nomerHp,
        this.alamatDelivery,
    });

    factory User.fromJson(String str) => User.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory User.fromMap(Map<String, dynamic> json) =>  User(
        userId: json["userId"] == null ? null : json["userId"],
        userName: json["userName"] == null ? null : json["userName"],
        displayName: json["displayName"] == null ? null : json["displayName"],
        status: json["status"] == null ? null : json["status"],
        isStaff: json["isStaff"] == null ? null : json["isStaff"],
        email: json["email"] == null ? null : json["email"],
        nomerHp: json["nomerHP"] == null ? null : json["nomerHP"],
        alamatDelivery: json["alamatDelivery"] == null ? null :  List<AlamatDelivery>.from(json["alamatDelivery"].map((x) => AlamatDelivery.fromMap(x))),
    );

    Map<String, dynamic> toMap() => {
        "userId": userId == null ? null : userId,
        "userName": userName == null ? null : userName,
        "displayName": displayName == null ? null : displayName,
        "status": status == null ? null : status,
        "isStaff": isStaff == null ? null : isStaff,
        "email": email == null ? null : email,
        "nomerHP": nomerHp == null ? null : nomerHp,
        "alamatDelivery": alamatDelivery == null ? null :  List<dynamic>.from(alamatDelivery.map((x) => x.toMap())),
    };
}


