// To parse this JSON data, do
//
//     final pesanan = pesananFromJson(jsonString);

import 'dart:convert';

import 'alamat_delivery.dart';
import 'meja.dart';
import 'menu.dart';

class Pesanan {
    String id;
    String type;
    List<Menu> menuPesanan;
    List<Meja> meja;
    int totalHarga;
    int diskon;
    int finalHarga;
    String userName;
    String userId;
    String processStatus;
    String paymentStatus;
    String createBy;
    String onCreate;
    String updateBy;
    String lastUpdate;
    int rating;
    String review;
    bool isNotif;
    String catatan;
    bool isCatering;
    OrderBy orderBy;
    AlamatDelivery deliverTo;
    String deliveryStatus;
    String deliverAt;
    String deliverBy;
    bool isResevasi;
    String tanggalCheckIn;
    String tanggalCheckOut;
    String durasiReservasi;

    Pesanan({
        this.id,
        this.type,
        this.menuPesanan,
        this.meja,
        this.totalHarga,
        this.diskon,
        this.finalHarga,
        this.userName,
        this.userId,
        this.processStatus,
        this.paymentStatus,
        this.createBy,
        this.onCreate,
        this.updateBy,
        this.lastUpdate,
        this.rating,
        this.review,
        this.isNotif,
        this.catatan,
        this.isCatering,
        this.orderBy,
        this.deliverTo,
        this.deliveryStatus,
        this.deliverAt,
        this.deliverBy,
        this.isResevasi,
        this.tanggalCheckIn,
        this.tanggalCheckOut,
        this.durasiReservasi,
    });

    factory Pesanan.fromJson(String str) => Pesanan.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Pesanan.fromMap(Map<String, dynamic> json) =>  Pesanan(
        id: json["id"] == null ? null : json["id"],
        type: json["type"] == null ? null : json["type"],
        menuPesanan: json["menuPesanan"] == null ? null :  List<Menu>.from(json["menuPesanan"].map((x) => Menu.fromMap(x.cast<String,dynamic>()))),
        meja: json["meja"] == null ? null :  List<Meja>.from(json["meja"].map((x) => Meja.fromMap(x.cast<String,dynamic>()))),
        totalHarga: json["totalHarga"] == null ? null : json["totalHarga"],
        diskon: json["diskon"] == null ? null : json["diskon"],
        finalHarga: json["finalHarga"] == null ? null : json["finalHarga"],
        userName: json["userName"] == null ? null : json["userName"],
        userId: json["userId"] == null ? null : json["userId"],
        processStatus: json["processStatus"] == null ? null : json["processStatus"],
        paymentStatus: json["paymentStatus"] == null ? null : json["paymentStatus"],
        createBy: json["createBy"] == null ? null : json["createBy"],
        onCreate: json["onCreate"] == null ? null : json["onCreate"],
        updateBy: json["updateBy"] == null ? null : json["updateBy"],
        lastUpdate: json["lastUpdate"] == null ? null : json["lastUpdate"],
        rating: json["rating"] == null ? null : json["rating"],
        review: json["review"] == null ? null : json["review"],
        isNotif: json["isNotif"] == null ? null : json["isNotif"],
        catatan: json["catatan"] == null ? null : json["catatan"],
        isCatering: json["isCatering"] == null ? null : json["isCatering"],
        orderBy: json["orderBy"] == null ? null : OrderBy.fromMap(json["orderBy"]),
        deliverTo: json["deliverTo"] == null ? null : AlamatDelivery.fromMap(json["deliverTo"].cast<String,dynamic>()),
        deliveryStatus: json["deliveryStatus"] == null ? null : json["deliveryStatus"],
        deliverAt: json["deliverAt"] == null ? null : json["deliverAt"],
        deliverBy: json["deliverBy"] == null ? null : json["deliverBy"],
        isResevasi: json["isResevasi"] == null ? null : json["isResevasi"],
        tanggalCheckIn: json["tanggalCheckIn"] == null ? null : json["tanggalCheckIn"],
        tanggalCheckOut: json["tanggalCheckOut"] == null ? null : json["tanggalCheckOut"],
        durasiReservasi: json["durasiReservasi"] == null ? null : json["durasiReservasi"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "type": type == null ? null : type,
        "menuPesanan": menuPesanan == null ? null :  List<dynamic>.from(menuPesanan.map((x) => x.toMap())),
        "meja": meja == null ? null :  List<dynamic>.from(meja.map((x) => x.toMap())),
        "totalHarga": totalHarga == null ? null : totalHarga,
        "diskon": diskon == null ? null : diskon,
        "finalHarga": finalHarga == null ? null : finalHarga,
        "userName": userName == null ? null : userName,
        "userId": userId == null ? null : userId,
        "processStatus": processStatus == null ? null : processStatus,
        "paymentStatus": paymentStatus == null ? null : paymentStatus,
        "createBy": createBy == null ? null : createBy,
        "onCreate": onCreate == null ? null : onCreate,
        "updateBy": updateBy == null ? null : updateBy,
        "lastUpdate": lastUpdate == null ? null : lastUpdate,
        "rating": rating == null ? null : rating,
        "review": review == null ? null : review,
        "isNotif": isNotif == null ? null : isNotif,
        "catatan": catatan == null ? null : catatan,
        "isCatering": isCatering == null ? null : isCatering,
        "orderBy": orderBy == null ? null : orderBy.toMap(),
        "deliverTo": deliverTo == null ? null : deliverTo.toMap(),
        "deliveryStatus": deliveryStatus == null ? null : deliveryStatus,
        "deliverAt": deliverAt == null ? null : deliverAt,
        "deliverBy": deliverBy == null ? null : deliverBy,
        "isResevasi": isResevasi == null ? null : isResevasi,
        "tanggalCheckIn": tanggalCheckIn == null ? null : tanggalCheckIn,
        "tanggalCheckOut": tanggalCheckOut == null ? null : tanggalCheckOut,
        "durasiReservasi": durasiReservasi == null ? null : durasiReservasi,
    };
}

class OrderBy {
    String nama;
    String userId;
    String nomorHp;

    OrderBy({
        this.nama,
        this.userId,
        this.nomorHp,
    });

    factory OrderBy.fromJson(String str) => OrderBy.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory OrderBy.fromMap(Map<String, dynamic> json) => new OrderBy(
        nama: json["nama"] == null ? null : json["nama"],
        userId: json["userId"] == null ? null : json["userId"],
        nomorHp: json["nomorHp"] == null ? null : json["nomorHp"],
    );

    Map<String, dynamic> toMap() => {
        "nama": nama == null ? null : nama,
        "userId": userId == null ? null : userId,
        "nomorHp": nomorHp == null ? null : nomorHp,
    };
}
