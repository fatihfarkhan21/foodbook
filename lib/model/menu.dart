// To parse this JSON data, do
//
//     final menu = menuFromJson(jsonString);

import 'dart:convert';


class Menu {
    String id;
    String namaNemu;
    String deskripsi;
    String kategori;
    bool isPromo;
    int harga;
    String status;
    String image;
    int jumlah;
    String catatan;
    bool isDineIn;
    bool isCatering;
    bool isSoldOut;

    Menu({
        this.id,
        this.namaNemu,
        this.deskripsi,
        this.kategori,
        this.isPromo,
        this.harga,
        this.status,
        this.image,
        this.jumlah,
        this.catatan,
        this.isDineIn,
        this.isCatering,
        this.isSoldOut,
    });

    factory Menu.fromJson(String str) => Menu.fromMap(json.decode(str));

    String toJson() => json.encode(toMap());

    factory Menu.fromMap(Map<String, dynamic> json) =>  Menu(
        id: json["id"] == null ? null : json["id"],
        namaNemu: json["namaNemu"] == null ? null : json["namaNemu"],
        deskripsi: json["deskripsi"] == null ? null : json["deskripsi"],
        kategori: json["kategori"] == null ? null : json["kategori"],
        isPromo: json["isPromo"] == null ? null : json["isPromo"],
        harga: json["harga"] == null ? null : json["harga"],
        status: json["status"] == null ? null : json["status"],
        image: json["image"] == null ? null : json["image"],
        jumlah: json["jumlah"] == null ? null : json["jumlah"],
        catatan: json["catatan"] == null ? null : json["catatan"],
        isDineIn: json["isDineIn"] == null ? null : json["isDineIn"],
        isCatering: json["isCatering"] == null ? null : json["isCatering"],
        isSoldOut: json["isSoldOut"] == null ? null : json["isSoldOut"],
    );

    Map<String, dynamic> toMap() => {
        "id": id == null ? null : id,
        "namaNemu": namaNemu == null ? null : namaNemu,
        "deskripsi": deskripsi == null ? null : deskripsi,
        "kategori": kategori == null ? null : kategori,
        "isPromo": isPromo == null ? null : isPromo,
        "harga": harga == null ? null : harga,
        "status": status == null ? null : status,
        "image": image == null ? null : image,
        "jumlah": jumlah == null ? null : jumlah,
        "catatan": catatan == null ? null : catatan,
        "isDineIn": isDineIn == null ? null : isDineIn,
        "isCatering": isCatering == null ? null : isCatering,
        "isSoldOut": isSoldOut == null ? null : isSoldOut,
    };
}