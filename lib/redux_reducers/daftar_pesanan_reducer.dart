import 'package:foodbook/redux_actions/daftar_pesanan.dart';
import 'package:foodbook/redux_states/daftar_pesanan_state.dart';
import 'package:redux/redux.dart';

final daftarPesananReducer = combineReducers<DaftarPesananState>([
  TypedReducer<DaftarPesananState, LoadDaftarPesanan>(_loadDaftarMenu),
  TypedReducer<DaftarPesananState, LoadDaftarPesananDone>(_updateDaftarMenu),
]);

DaftarPesananState _loadDaftarMenu(DaftarPesananState daftarPesananState, LoadDaftarPesanan action){
  return DaftarPesananLoading();
}

DaftarPesananState _updateDaftarMenu(DaftarPesananState daftarPesananState, LoadDaftarPesananDone action){
  return DaftarPesananLoaded(daftarPesanan: action.daftarMenu);
}