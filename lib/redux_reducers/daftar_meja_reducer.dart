import 'package:foodbook/redux_actions/daftar_meja_action.dart';
import 'package:foodbook/redux_states/daftar_meja_state.dart';
import 'package:redux/redux.dart';

final daftarMejaReducer = combineReducers<DaftarMejaState>([
  TypedReducer<DaftarMejaState, LoadDaftarMeja>(_loadDaftarMeja),
  TypedReducer<DaftarMejaState, LoadDaftarMejaDone>(_updateDaftarMeja),
]);

DaftarMejaState _loadDaftarMeja(DaftarMejaState daftarMejaState, LoadDaftarMeja action){
  return DaftarMejaLoading();
}

DaftarMejaState _updateDaftarMeja(DaftarMejaState daftarMejaState, LoadDaftarMejaDone action){
  return DaftarMejaLoaded(daftarMeja: action.daftarMeja);
}
