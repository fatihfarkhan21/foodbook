import 'package:foodbook/redux_actions/auth_action.dart';
import 'package:foodbook/redux_states/auth_state.dart';
import 'package:redux/redux.dart';

final authReducer = combineReducers<AuthState>([
  TypedReducer<AuthState, AuthInit>(_authInitHandle),
  TypedReducer<AuthState, AuthWithGoogle>(_authWithGoogleHandle),
  TypedReducer<AuthState, AuthSuccess>(_authSuccessHandle),
  TypedReducer<AuthState, LogOut>(_logoutHandle),
  TypedReducer<AuthState, AuthFail>(_authFail),
  TypedReducer<AuthState, UpdateProfileAction>(_updateProfile),
]);

AuthState _authInitHandle(AuthState authState, AuthInit action) {
  return AuthenticationLoading();
}

AuthState _authWithGoogleHandle(AuthState authState, AuthWithGoogle action) {
  return AuthenticationLoading();
}

AuthState _authSuccessHandle(AuthState authState, AuthSuccess action) {
  return AuthenticationAuthenticated(user: action.userProfile);
}

AuthState _logoutHandle(AuthState authState, LogOut action) {
  return AuthenticationUnauthenticated();
}

AuthState _authFail(AuthState authState, AuthFail action) {
  return AuthenticationError(errorInfo: action.error);
}

AuthState _updateProfile(AuthState authState, UpdateProfileAction action) {
  return AuthenticationAuthenticated(user: action.user);
}
