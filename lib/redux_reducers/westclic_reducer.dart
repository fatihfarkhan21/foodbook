
import 'package:foodbook/redux_states/westclic_state.dart';
import 'daftar_pesanan_reducer.dart';
import 'order_reducer.dart';
import 'redux_reducers.dart';


WestclicState westclicReducer(WestclicState westclicState, action ){
  //print('Action  : ${action.runtimeType.toString()}');
  return WestclicState(
    authState: authReducer(westclicState.authState, action),
    daftarMejaState: daftarMejaReducer(westclicState.daftarMejaState, action),
    daftarMenuState: daftarMenuReducer(westclicState.daftarMenuState, action),
    daftarPesananState: daftarPesananReducer(westclicState.daftarPesananState, action),
    orderState: orderReducer(westclicState.orderState, action)
  );
}

