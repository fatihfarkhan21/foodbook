import 'package:foodbook/model/meja.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/order_action.dart';
import 'package:foodbook/redux_states/order_state.dart';
import 'package:redux/redux.dart';

final orderReducer = combineReducers<OrderState>([
  TypedReducer<OrderState, CreateOrder>(_createOrder),
  TypedReducer<OrderState, OrderUpdate>(_updateOrder)
]);

OrderState _createOrder(OrderState orderState, CreateOrder action){

  Pesanan pesanan = Pesanan();
  pesanan.menuPesanan = <Menu>[];
  pesanan.meja = <Meja>[];

  return NewOrder(pesanan);
}

OrderState _updateOrder(OrderState orderState, OrderUpdate action){
  return OrderUpdated(action.pesanan);
}