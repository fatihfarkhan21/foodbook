import 'package:foodbook/redux_actions/daftar_menu_action.dart';
import 'package:foodbook/redux_states/daftar_menu_state.dart';
import 'package:redux/redux.dart';

final daftarMenuReducer = combineReducers<DaftarMenuState>([
  TypedReducer<DaftarMenuState, LoadDaftarMenu>(_loadDaftarMenu),
  TypedReducer<DaftarMenuState, LoadDaftarMenuDone>(_updateDaftarMenu),
]);

DaftarMenuState _loadDaftarMenu(DaftarMenuState daftarMenuState, LoadDaftarMenu action){
  return DaftarMenuLoading();
}

DaftarMenuState _updateDaftarMenu(DaftarMenuState daftarMejaState, LoadDaftarMenuDone action){
  return DaftarMenuLoaded(daftarMenu: action.daftarMenu);
}