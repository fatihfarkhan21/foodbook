import 'package:flutter/material.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/model/model.dart';
import 'package:foodbook/redux_actions/redux_actions.dart';
import 'package:foodbook/redux_states/redux_states.dart';

class TestRepo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var st = StoreProvider.of<WestclicState>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text("test dev"),
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.add_box),
                  label: Text('Add Meja'),
                  onPressed: (){
                    Meja mj = Meja(
                      deskripsi: "tambahan baru",
                      isAvailable: true,
                      nomorMeja: "3",
                      kapasitas: 2,
                    );
                    st.dispatch(AddMeja(meja: mj));
                  },
                ),
              ],
            ),
            Expanded(
              child: StoreBuilder<WestclicState>(
                onInit: (store) => store.dispatch(LoadDaftarMeja()),
                builder: (context, store){
                  if(store.state.daftarMejaState is DaftarMejaNotLoaded || store.state.daftarMejaState is DaftarMejaLoading ) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }else{
                    var dm = (store.state.daftarMejaState as DaftarMejaLoaded).daftarMeja;
                    return ListView.builder(
                      itemCount: dm.length,
                      itemBuilder: (BuildContext ctxt, index) {
                        return ListTile(
                          leading: Text(dm[index].nomorMeja),
                          title: Text(dm[index].deskripsi),
                          subtitle: Text(dm[index].id == null ? '': dm[index].id),
                          trailing: IconButton(
                            icon: Icon(Icons.delete_forever),
                            onPressed: (){
                              store.dispatch(DeleteMeja(meja: dm[index]));
                            },
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
            Row(
              children: <Widget>[
                FlatButton.icon(
                  icon: Icon(Icons.add_box),
                  label: Text('Add Menu'),
                  onPressed: (){
                    Menu menu = Menu(
                      jumlah: 1,
                      namaNemu: 'Telo Gosong',
                      deskripsi: 'Jozz buat kehidupan pahit',
                      harga: 3000
                    );

                    st.dispatch(AddMenu(menu: menu));

                  },
                ),
              ],
            ),
            Expanded(
              child: StoreBuilder<WestclicState>(
                onInit: (store) => store.dispatch(LoadDaftarMenu()),
                builder: (context, store){
                  if(store.state.daftarMejaState is DaftarMenuNotLoaded || store.state.daftarMejaState is DaftarMenuLoading ) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }else{
                    var dm = (store.state.daftarMenuState as DaftarMenuLoaded).daftarMenu;
                    return ListView.builder(
                      itemCount: dm.length,
                      itemBuilder: (BuildContext ctxt, index) {
                        return ListTile(
                          leading: Text(dm[index].jumlah.toString()),
                          title: Text(dm[index].namaNemu),
                          subtitle: Text(dm[index].deskripsi + ' ' + dm[index].harga.toString()),
                          trailing: IconButton(
                            icon: Icon(Icons.delete_forever),
                            onPressed: (){
                              store.dispatch(DeleteMenu(menu: dm[index]));
                            },
                          ),
                        );
                      },
                    );
                  }
                },
              ),
            ),
          ],
        )
      ),
    );
  }
}
