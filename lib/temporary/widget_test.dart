import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:foodbook/view/widget/staff_widget/card_menu.dart';

class FormTest extends StatefulWidget {
  @override
  _FormTestState createState() => _FormTestState();
}

class _FormTestState extends State<FormTest> {
  @override
  Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text("Test")
      ),
      body: Container(
        child: Column(
          children: [
            CardMenu()
          ],
        ),
      ),
    );
  }
}