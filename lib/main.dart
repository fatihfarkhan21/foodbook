import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_redux/flutter_redux.dart';
import 'package:foodbook/redux_middlewares/westclic_middleware.dart';
import 'package:foodbook/view/widget/staff_widget/details_menu.dart';
import 'package:redux/redux.dart';

import 'redux_reducers/redux_reducers.dart';
import 'redux_states/redux_states.dart';
import 'temporary/widget_test.dart';
import 'view/view.dart';

void main() {
  final westclicStore = Store<WestclicState>(
    westclicReducer,
    initialState: WestclicState.loading(),
    middleware: westclicMinddleware,
  );

  runApp(MyApp(
    westclicStore: westclicStore,
  ));
}

class MyApp extends StatelessWidget {
  final Store<WestclicState> westclicStore;

  MyApp({Key key, this.westclicStore}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return StoreProvider<WestclicState>(
      store: westclicStore,
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'WestClic App',
          theme: Theme.of(context).copyWith(
            appBarTheme: Theme.of(context)
                .appBarTheme
                .copyWith(brightness: Brightness.light),
          ),
          //home: AppPage(),
          home: LoginPage()
          // home: InputMenuPage(),
          //home: Login(),
          // home: SplashScreen(),
          ),
    );
  }
}
